<%@ page language="java" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="s" uri="/struts-tags" %>

<html>
<head>
  <title>Web memberships</title>
<s:head theme="simple"/>  
</head>
<link href="<s:url value="/css/main.css"/>" rel="stylesheet" type="text/css"/>

<body>

<h2>Search RACT Interim Web Memberships</h2>

<s:form action="searchTemporaryMembership" theme="simple">
<table>
<tr>
<td>Interim Membership No.:</td>
<td><s:textfield name="tempMembershipNo"></s:textfield></td>
<td><s:submit value="Search"></s:submit></td>
</tr>
<tr>
<td>
<s:actionerror/>
</td>
</tr>
</table>

<!-- results of the search  -->

<s:if test="webMembershipTransaction != null && searched">

<p>Interim membership status as at <s:date name="now" format="dd/MM/yyyy HH:mm:ss"/></p> 

<table border="1">

<thead>
<tr>
<th>Interim Membership No.</th><th>Name</th><th>Transaction Date</th><th>Membership Type</th><th>Eligible for Road Service?</th><th>Start Date</th><th>End Date</th><th>Road Service Action</th>
</tr>
</thead>

<tbody>
<tr>
<td><s:property value="tempMembershipNo"/></td><td><s:property value="displayName"/></td><td><s:date name="webMembershipTransaction.createDate" format="dd/MM/yyyy HH:mm:ss"/></td><td><s:property value="webMembershipTransaction.productCode"/></td><td><b><s:property value="eligibleForRoadservice"/></b></td><td><s:date name="webMembershipTransaction.startDate" format="dd/MM/yyyy HH:mm:ss"/></td><td><s:date name="webMembershipTransaction.endDate" format="dd/MM/yyyy HH:mm:ss"/></td><td><b><s:property value="memberAction"/></b></td>
</tr>
</tbody>

</table>

</s:if>
<s:elseif test="webMembershipTransaction == null && searched">

No temporary membership found.

</s:elseif>

</s:form>
    
</body>
  
</html>
