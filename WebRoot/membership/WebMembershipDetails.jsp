<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sx" uri="/struts-dojo-tags"%>

<html>

<head>
<link href="<s:url value="/css/main.css"/>" rel="stylesheet"
	type="text/css" />
<title>Web Membership Details</title>
<sx:head />
</head>

<script type="text/javascript">
	function showClient(cltNo) {
		document.getElementById("webClientNo").value = cltNo;
		dojo.event.topic.publish("showDet");
	}

	function matchClient(cltNo) {
		var url = "getRactClient.action?webClientNo=" + cltNo;
		window.open(url, '', 'titlebar=yes,toolbar=no,status=no,scrollbars=yes,resizable=yes');
		return true;
	}
	
	function handleReturnedData() {
		var details = document.getElementById("pListener").value;
		var detArray = details.split("|");
		var webClientNo = detArray[0];
		var ractClientNo = detArray[1];
		showClient(webClientNo);
	}
	
	/**************************************************************/
	var alreadyConverted = false;
	var req = null;
	var console = null;
	var READY_STATE_UNINITIALIZED = 0;
	var READY_STATE_LOADING = 1;
	var READY_STATE_LOADED = 2;
	var READY_STATE_INTERACTIVE = 3;
	var READY_STATE_COMPLETE = 4;
	function convertMembership() {
		document.getElementById('convertBtn').disabled = true;
		if (alreadyConverted == true) {
			alert("Membership already created");
		} else {
			var webMembershipNo = document.getElementById("webMembershipNo").value;
			sendRequest("convertMembership.action", "webMembershipNo="
					+ webMembershipNo, "POST");
		}
	}

	function sendRequest(url, params, HttpMethod) {
		if (!HttpMethod) {
			HttpMethod = "GET";
		}
		req = initXMLHTTPRequest();

		if (req) {
			req.onreadystatechange = onReadyState;
			req.open(HttpMethod, url, true);
			req.setRequestHeader("Content-Type",
					"application/x-www-form-urlencoded");
			req.send(params);
		}
	}
	
	function initXMLHTTPRequest() {
		var xRequest = null;
		if (window.XMLHttpRequest) {
			xRequest = new XMLHttpRequest();
		} else if (window.ActiveXObject) {
			xRequest = new ActiveXObject("Microsoft.XMLHTTP");
		}
		return xRequest;
	}
	
	function onReadyState() {
		var ready = req.readyState;
		var jsonObject = null;

		if (ready == READY_STATE_COMPLETE) {
			alert(req.responseText);
			if (req.responseText == "Success") {
				/* nobble the button so it can't be re-used.*/
				alreadyConverted = true;
			}
			document.getElementById('convertBtn').disabled = false;
		}
	}
</script>
<body>

	<%@include file="/menu/PopupHeader.jsp"%>

	<h2>
		Web Membership
		<s:property value="webMembershipTransactionHeader.transactionHeaderId" />
	</h2>

	<table cellspacing="5" cellpadding="5">
		<tr>
			<td valign="top">
				<!-- left side of page --> <s:include
					value="/membership/WebMembershipDetailsInclude.jsp"></s:include>

				<table>
					<thead>
						<tr>
							<th colspan="4" align="left"><h3>Payment Details</h3></th>
						</tr>
					</thead>
					<tbody>
						<s:if test="%{webMembershipPayment.paymentType == 'DirectDebit'}">
							<s:if test="%{webMembershipPayment.bsb != null}">
								<tr>
									<td>BSB</td>
									<td><b><s:property value="webMembershipPayment.bsb" /></b></td>
								</tr>
								<tr>
									<td>Account Number</td>
									<td><b>Securely Stored</b></td>
								</tr>
								<tr>
									<td>Account Name</td>
									<td><b><s:property
										value="webMembershipPayment.accountName" />
									</b></td>
								</tr>
							</s:if>
							<s:else>
								<tr>
									<td>Card Number</td>
									<!--           <s:set name="obfuscatedCardNumber" value="%{webMembershipPayment.cardNumber.substring(0,4) + '********' + webMembershipPayment.cardNumber.substring(12,16)}" />-->
									<!--           <td><b><s:property value="%{obfuscatedCardNumber}"/></b></td> -->
									<td><b>Securely stored</b></td>
									<td>Expiry Date</td>
									<td><b><s:property
										value="webMembershipPayment.expiryDate" />
									</b>
									</td>
								</tr>
								<tr>
									<td>Card Holder</td>
									<td><b><s:property
										value="webMembershipPayment.cardHolder" />
									</b>
									</td>
									<td>Card Type</td>
									<td><b><s:property value="webMembershipPayment.cardType" /></b></td>
								</tr>
							</s:else>
						</s:if>
						<s:else>
						<tr>
							<td>Card Number</td>
							<!--           <s:set name="obfuscatedCardNumber" value="%{webMembershipPayment.cardNumber.substring(0,4) + '********' + webMembershipPayment.cardNumber.substring(12,16)}" />-->
							<!--           <td><b><s:property value="%{obfuscatedCardNumber}"/></b></td> -->
							<td><b>Securely stored</b></td>
							<td>Expiry Date</td>
							<td><b><s:property
								value="webMembershipPayment.expiryDate" />
							</b>
							</td>
						</tr>
						<tr>
							<td>Card Holder</td>
							<td><b><s:property
								value="webMembershipPayment.cardHolder" />
							</b>
							</td>
							<td>Card Type</td>
							<td><b><s:property value="webMembershipPayment.cardType" /></b></td>
						</tr>
						</s:else>
					</tbody>
				</table></td>
			<td valign="top">
				<!-- right side of page -->
				<h3>
					MembershipTransactions<br>
				</h3>
				<table>
					<tr class="heading">
						<td>Transaction Id</td>
						<td>Transaction Type</td>
						<td>Start Date</td>
						<td>End Date</td>
						<td>Product Code</td>
						<td>Frequency</td>
					</tr>
					<s:iterator value="webMembershipTransactionList" status="cStat">
						<tr
							class="<s:if test="#cStat.odd == false">odd</s:if><s:else>even</s:else>">
							<td><s:property
									value="webMembershipTransactionPK.transactionId" />
							</td>
							<td><s:property value="transactionTypeCode" /></td>
							<td><s:date name="startDate" format="dd/MM/yyyy"/></td>
							<td><s:date name="endDate" format="dd/MM/yyyy" /></td>
							<td><s:property value="productCode" /></td>
							<td><s:property value="membershipTerm" /></td>
						</tr>

						<tr>
							<td colspan="5">
								<table>
									<s:if test="webClientNo != null && webClientNo != -1">
										<tr>
											<td colspan="3"><b>Client</b></td>
										</tr>
										<tr class="heading">
											<td>Web Client No</td>
											<td>Name</td>
											<td>RACT Client No</td>
										</tr>
										<tr>
											<td><a
												href="javascript:showClient('<s:property value="webClientNo"/>')"><s:property value="webClientNo" />
											</a>
											</td>
											<td>
												<s:property value="webClient.title" /> <s:property value="webClient.givenNames" /> 
												<s:property value="webClient.surname" />
											</td>
											<td><s:property value="webClient.ractClientNo" /></td>
										</tr>
									</s:if>
									<s:elseif test="">
										<tr>
											<td>No clients attached.</td>
										</tr>
									</s:elseif>
								</table></td>
						</tr>
					</s:iterator>
				</table> <s:url id="clientUrl" action="showAClient" /> <sx:div
					showLoadingText="false" id="clientDetails" href="%{clientUrl}"
					theme="ajax" listenTopics="showDet" formId="detFrm">
				</sx:div></td>
		</tr>
	</table>

	<s:set name="encrypt_error" value="false"/>
	<s:if test="hasActionErrors()">
        <s:iterator value="actionErrors">
            <s:if test="top.indexOf('encryption') > 0">
				<s:set name="encrypt_error" value="true"/>
            </s:if>
        </s:iterator>
	</s:if>
	<table border="0">
		<tr>
			<td nowrap="nowrap">
				<s:form id="detFrm" theme="simple"
					action="convertWebMembership">
					<s:hidden name="webMembershipNo" id="webMembershipNo" value="%{webMembershipTransactionHeader.transactionHeaderId}" />
					<s:hidden name="webClientNo" id="webClientNo" value="" />
					<input type="hidden" name="pListener" onclick="handleReturnedData()" />
					<s:if test="%{#encrypt_error == false}">
						<s:if test="webMembershipTransactionHeader.completedDate != null && webMembershipTransactionHeader.userName.equalsIgnoreCase(user.userID)">
							<input type="button" name="convertButton" id="convertBtn" value="Convert Membership" onclick="convertMembership()" />
						</s:if>
					</s:if>
					<s:property value="conversionResponse" />
				</s:form></td>
			<td>
				<s:if test="%{#encrypt_error == false}">
					<s:if test="owner || administrator">
						<s:form action="updateWebMembership" theme="simple">
							<s:hidden name="webMembershipNo" id="webMembershipNo" value="%{webMembershipTransactionHeader.transactionHeaderId}" />
							<s:submit value="Update Web Membership"></s:submit>
						</s:form>
					</s:if>
				</s:if>
				</td>
			<td>
				<s:form action="showMemberships" theme="simple">
					<s:submit value="View Web Membership List"></s:submit>
				</s:form>
			</td>
		</tr>
	</table>

	<s:actionerror/>

</body>
</html>
