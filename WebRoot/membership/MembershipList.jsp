<%@ page language="java" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sx" uri="/struts-dojo-tags"%>

<html>
<head>
  <title>Web memberships</title>
<sx:head/>  
</head>
<link href="<s:url value="/css/main.css"/>" rel="stylesheet" type="text/css"/>
<script type="text/javascript">
function show_details()
{
     dojo.event.topic.publish("show_detail");
}
</script>
<body>
  
<s:include value="/menu/PopupHeader.jsp"></s:include>
<s:form id="frm" theme="simple">
<h1>Web Memberships</h1>
<table>
  <tr>
    <td> 
       Web Transaction Reference:
    </td>
    <td>
       <s:textfield name="webMembershipNo" id="webMembershipNo">
       </s:textfield>
    </td>
    <td>
        <input type="button" value="Search"  
               onclick="show_details(); return;">
    </td>
  </tr>
</table>  
<s:actionerror/>
<s:url id="actionUrl" action="showMembershipList" />
<sx:div showLoadingText="false" 
       id="list"
       href="%{actionUrl}" theme="ajax"
       listenTopics="show_detail" formId="frm">
</sx:div>
 
 </s:form>
</body>
</html>

