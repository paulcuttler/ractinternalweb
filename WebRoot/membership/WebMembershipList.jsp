<%@ taglib prefix="s" uri="/struts-tags"%>
<%@page import="com.ract.membership.MembershipTransactionTypeVO"%>

<s:if test="membershipList != null">
    <table border="1" cellspacing="2" cellpadding="2">
    <tr>
       <td>Web Transaction Reference</td>
       <td>Completion Date</td>
       <td>Transaction Type</td>
       <td>Amount</td>   
       <td>Status</td>
       <td>Branch</td>
       <td>User</td>
    </tr>  
  <s:iterator value="membershipList" id="list" status="lstat">
     <tr class="<s:if test="#lstat.odd == true">odd</s:if><s:else>even</s:else>">        
        <td>
        <s:url action="showMembership" id="membership_url_id">
                 <s:param name="transactionHeaderId" value="%{transactionHeaderId}"/>
                 <s:param name="userName" value="%{userName}"/>
        </s:url>
           <s:a href="%{membership_url_id}">
           <s:property value="transactionHeaderId"/>
           </s:a>
        </td>
        <td><s:date name="completedDate" format="dd/MM/yyyy HH:mm:ss"/></td>
        <td>Individual</td>
        <td><s:text name="format.money"><s:param name="value" value="amountPayable"/></s:text></td>
        <td><s:property value="conversionStatus"/></td>
        <td><s:property value="salesBranch"/></td>
        <td><s:property value="userName"/></td>
     </tr>
   
  </s:iterator>
  </table>
</s:if>
  <s:actionerror />
  <s:fielderror /> 


