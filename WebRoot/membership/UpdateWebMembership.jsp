<%@ page language="java" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="s" uri="/struts-tags" %>

<html>
<head>
  <title>Web memberships</title>
<s:head/>  
</head>
<link href="<s:url value="/css/main.css"/>" rel="stylesheet" type="text/css"/>

<body>

<h2>Update Web Membership</h2>

<!-- include header details -->
<s:include value="/membership/WebMembershipDetailsInclude.jsp"></s:include>


<s:form action="saveWebMembership" theme="simple">
<s:hidden name="webMembershipNo" value="%{webMembershipTransactionHeader.transactionHeaderId}"/>
<table>
<tr>
<td>Conversion Status</td>
<td><s:select label="Conversion Status" name="conversionStatus" list="conversionStatusList"></s:select></td>
</tr>
<tr>
<td>Allocated User</td>
<s:if test="administrator">
<td><s:select emptyOption="true" label="Allocated User" name="allocatedUserId" listKey="userID" listValue="username" list="allocatedUserList"></s:select></td>
</s:if>
<s:else>
<td><s:property value="allocatedUser"/> </td>
<s:hidden name="allocatedUserId"></s:hidden>
</s:else>
</tr>
<tr>
<td>Allocated Branch</td>
<s:if test="administrator">
<td><s:select emptyOption="true" label="Allocated Branch" name="salesBranchCode" listKey="salesBranchCode" listValue="salesBranchName" list="allocatedBranchList"></s:select></td>
</s:if>
<s:else>
<td><s:property value="salesBranch"/> </td>
<s:hidden name="salesBranchCode"></s:hidden>
</s:else>
</tr>
<tr>
<td>Receipt Number</td>
<td><s:textfield name="conversionReference"></s:textfield> </td>
</tr>
<tr>
<td>Conversion Comments</td>
<td><s:textarea name="conversionComments" rows="5" cols="20"></s:textarea></td>
</tr>
<tr>
<td><s:submit value="Save"></s:submit></td>
<td></td>
</tr>
</table>
<s:actionerror/>
</s:form>

</body>

</html>
