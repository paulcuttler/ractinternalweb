<%@ taglib prefix="s" uri="/struts-tags" %>     
<table border="0" cellspacing="0" cellpadding="4">
   <thead>
   <tr>
   <th colspan="4" align="left"><h3>Membership Header Details</h3>
   </th>
   </tr>
   </thead>      
   <tr>
     <td>Transaction Header Id</td>
     <td><b><s:property value="webMembershipTransactionHeader.transactionHeaderId"/></b></td>      
   <td>Conversion Status</td>
   <td><b><s:property value="webMembershipTransactionHeader.conversionStatus"/></b></td>
 </tr>
 <tr> 
   <td>Create Date</td>
   <td><b><s:date name="webMembershipTransactionHeader.createDate" format="dd/MM/yyyy HH:mm:ss"/></b></td>
   <td>Last Update</td>
   <td><b><s:date name="webMembershipTransactionHeader.lastUpdate" format="dd/MM/yyyy HH:mm:ss"/></b></td>
 </tr>
 <tr> 
   <td>Group Transaction Type</td>
   <td><b><s:property value="webMembershipTransactionHeader.transactionGroupTypeCode"/></b></td>
   <td>Gross Transaction Fee</td>
   <td><b><s:text name="format.money"><s:param name="value" value="webMembershipTransactionHeader.grossTransactionFee"/></s:text></b></td>

 </tr>
 <tr> 
   <td>Amount Payable</td>
   <td><b><s:text name="format.money"><s:param name="value" value="webMembershipTransactionHeader.amountPayable"/></s:text></b></td>          
   <td>Discount Total</td>
   <td><b><s:text name="format.money"><s:param name="value" value="webMembershipTransactionHeader.discountTotal"/></s:text></b></td>          
 </tr>
 <tr> 
   <td>Adjustment Total</td>
   <td><b><s:text name="format.money"><s:param name="value" value="webMembershipTransactionHeader.adjustmentTotal"/></s:text></b></td>
   <td>Tax Total</td>
   <td><b><s:text name="format.money"><s:param name="value" value="webMembershipTransactionHeader.taxTotal"/></s:text></b></td>          
 </tr>        
 <tr> 
   <td>Completion Date</td>
   <td><b><s:date name="webMembershipTransactionHeader.completedDate" format="dd/MM/yyyy HH:mm:ss"/></b></td>
   <td>Conversion Date</td>
   <td><b><s:date name="webMembershipTransactionHeader.conversionDate" format="dd/MM/yyyy HH:mm:ss"/></b></td>
 </tr>
 <s:if test="webMembershipTransactionHeader.conversionDate != null">
  <tr> 
   <td>Receipt Number</td>
   <td><b><s:property value="webMembershipTransactionHeader.conversionReference"/></b></td>
   <td>Conversion Comments</td>
   <td><b><s:property value="webMembershipTransactionHeader.conversionComments"/></b></td>
 </tr>
 </s:if>
 <tr> 
   <td>User</td>
   <td><b><s:property value="allocatedUser"/></b></td>
   <td>Sales Branch</td>
   <td><b><s:property value="salesBranch"/></b></td>
 </tr>       
 <tr> 
   <td>Comments</td>
   <td colspan="3"><b><s:property value="webMembershipTransactionHeader.comments"/></b></td>
   <td></td><td></td>
 </tr>        
 </table>