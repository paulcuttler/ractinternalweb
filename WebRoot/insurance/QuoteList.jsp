<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sx" uri="/struts-dojo-tags"%>

<%@page import="com.ract.common.*"%>
<%@page import="com.ract.common.ui.*"%>
<%@page import="com.ract.user.*"%>
<%@page import="com.ract.util.*"%>

<html>
<head>
  <title>List quotes</title>
<sx:head/>
</head>
<link href="<s:url value="/css/main.css"/>" rel="stylesheet" type="text/css"/>
<script type="text/javascript">
function show_details()
{
     dojo.event.topic.publish("show_Detail");
}
/*
function init()
{
  var bBox = document.all.getElementById("status");
  bBox.focus();
}
dojo.addOnLoad(init);*/
</script>
<body>
<%@include file="/menu/PopupHeader.jsp"%>
<s:form id="frm" theme="simple">
<h1>Web Quotes</h1>
<table>
  <tr>
    <td>
       <font face="Arial, Helvetica, sans-serif">Quote No or Status</font>
    </td>
    <td>
  
       <sx:autocompleter name="status" id="status" 
                        list="{'All','bur','dev','est','ins','gln','hob','kng','lau','msc'}"
                        emptyOption="false"
                        value="">
       </sx:autocompleter>
        
     </td>    
    <td>
        <input type="button" value="Search"  
               onclick="show_details(); return;">
    </td>
  </tr>
</table>  
<s:actionerror />
<s:url id="actionUrl" action="showQuoteList" />
<sx:div showLoadingText="false" 
       id="list"
       href="%{actionUrl}" theme="ajax"
       listenTopics="show_Detail" formId="frm">
</sx:div>
 
 </s:form>
</body>
</html>
