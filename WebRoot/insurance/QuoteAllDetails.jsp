<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sx" uri="/struts-dojo-tags"%>

<html>

<head>
  <link href="<s:url value="/css/main.css"/>" rel="stylesheet" type="text/css"/>
  <title> QuoteAllDetails </title>
<sx:head/>
</head>
<script type="text/javascript">

  function showClient(cltNo)
  {
     document.getElementById("webClientNo").value = cltNo;
     dojo.event.topic.publish("showDet");
  }

  function matchClient(cltNo)
  {
  /*   var url = "ins/SetRactClient.jsp?webClientNo=" + cltNo;*/
     var url = "getRactClient.action?webClientNo=" + cltNo;
     window.open(url, '', 'titlebar=yes,toolbar=no,status=no,scrollbars=yes,resizable=yes');
     return true;
  }
  function handleReturnedData()
  {
     var details = document.getElementById("pListener").value;
     var detArray = details.split("|");
     var webClientNo = detArray[0];
     var ractClientNo = detArray[1];
     showClient(webClientNo);
  }
/**************************************************************/
var alreadyConverted = false;
var req=null;
var console=null;
var pMessage = "Button already pressed";
var READY_STATE_UNINITIALIZED=0;
var READY_STATE_LOADING=1;
var READY_STATE_LOADED=2;
var READY_STATE_INTERACTIVE=3;
var READY_STATE_COMPLETE=4;  
  function doPolicy(convType,riskType)
  {
     
     document.getElementById('convertBtn').disabled = true;    
     if(alreadyConverted==true)
     {
       alert(pMessage);
     }
     else
     {
        var printProposal = false;
        if(convType=="policy")
        {
           printProposal = confirm("Print Proposal?");
        }   
        alreadyConverted = true;
        var webQuoteNo=document.getElementById("webQuoteNo").value;
        var userId = document.getElementById("userId").value;
        sendRequest("convertPolicy.action", "webQuoteNo=" + webQuoteNo 
                                          + "&userId=" + userId
                                          + "&convType=" + convType
                                          + "&riskType=" + riskType
                                          + "&printProposal=" + printProposal,
                                           "POST");
     }
   } 
   
   function disableQuote () {
	 var webQuoteNo=document.getElementById("webQuoteNo").value;
     var userId = document.getElementById("userId").value;
     var disableReason = document.getElementById("disableReason").value;
     
     /*if (disableReason == '') {
     	alert("Please select a reason");
     	return;
     }*/
     
     sendRequest("disableQuote.action", "webQuoteNo=" + webQuoteNo
                                          + "&reason=" + disableReason
                                          + "&userId=" + userId,
                                           "POST");   
   
   }
   
   function sendRequest ( url, params, HttpMethod ) 
  {
	if ( !HttpMethod ){
		HttpMethod="GET";
	}
	req=initXMLHTTPRequest();	
		
	if ( req ) 
	{
		req.onreadystatechange=onReadyState;
		req.open(HttpMethod, url, true );
		req.setRequestHeader ( "Content-Type", "application/x-www-form-urlencoded");
		req.send (params);
	}
  }
  function initXMLHTTPRequest()
  {
	var xRequest=null;
	if (window.XMLHttpRequest) 
	{
		xRequest=new XMLHttpRequest();
	} 
	else if ( window.ActiveXObject )
	{
		xRequest = new ActiveXObject("Microsoft.XMLHTTP");
	}
	return xRequest;
  }
  function onReadyState() 
  {
	var ready=req.readyState;
	var jsonObject=null;

	if ( ready == READY_STATE_COMPLETE )
	{
	   alert(req.responseText);	
	   if(req.responseText.substr(0,7) == "Success")
	   {
	        /* nobble the button so it can't be re-used.*/
	     alreadyConverted=true;
	     pMessage = "Already converted";
	   }
	   else alreadyConverted = false;
	   document.getElementById('convertBtn').disabled = false;
	}	
  }  
     
     
     
</script>
<body>
<s:form id="detFrm" theme="simple" action="convertPolicy" >
<%@include file="/menu/PopupHeader.jsp"%>
<h2>Web Quote <s:property value="quote.getFormattedQuoteNo()"/></h2>
<s:hidden name="webQuoteNo" id="webQuoteNo" value="%{quote.webQuoteNo}"/>
<s:hidden name="userId" id="userId" value="%{userId}"/>
<table cellspacing="5" cellpadding="5">
  <tr>
     <td valign="top">
     <!-- left side of page -->
      <table border="0" cellspacing="0" cellpadding="4">
        <tr> 
          <td>Create Date</td>
          <td><b><s:property value="quote.createDate"/></b></td>
          <td/><td/>
        </tr>
        <tr> 
          <td>Quote Date</td>
          <td><b><s:property value="quote.quoteDate"/></b></td>
          <td>Start Cover</td>
          <td><b><s:property value="quote.startCover"/></b></td>          
        </tr>
        <tr> 
          <td>Quote Type</td>
          <td><b><s:property value="quote.quoteType"/></b></td>
          <td>Cover Type</td>
          <td><b><s:property value="quote.coverType"/></b></td>          
        </tr>
        <tr> 
          <td>Quote Status</td>
          <td><b><s:property value="quote.quoteStatus"/></b></td>
        </tr>
        </table>
      <table>
        <s:iterator value="quoteDetails" status="lstat">  
          <tr class="<s:if test="#lstat.odd == true">odd</s:if><s:else>even</s:else>">
            <td><s:property value="fieldName"/></td>
            <td><b><s:property value="fieldValue"/></b></td>
         
          </tr>
        </s:iterator>
      
      </table>
     </td>
     <td valign="top">
     <!-- right side of page -->
          <h3>Owners and Drivers</h3>      
          <table>
            <tr class="heading">
               <td>Given Names</td>
	           <td>Surname</td>
	           <td>Owner?</td>
	           <td>Driver?</td>
	           <td>Preferred<br/> Address?</td>
	           <td>RACT <br/>Client No  </td> 
	           <td>&nbsp;</td>           
            </tr>
            <s:iterator value="clientList" status="cStat">
               <tr onclick="showClient('<s:property value="webClientNo"/>')" class="<s:if test="#cStat.odd == false">oddLink</s:if><s:else>evenLink</s:else>">
		               <td><s:property value="givenNames"/></td>
		               <td><s:property value="surname"/></td>
		               <td><s:property value="owner"/></td>
		               <td><s:property value="driver"/></td>
		               <td><s:property value="preferredAddress"/></td>
		               <td><s:property value="ractClientNo"/></td>
		               <td><s:property value="tData"/></td>	
               </tr>
            </s:iterator> 
          </table>
          <s:url id="clientUrl" action="showAClient" />
          <sx:div showLoadingText="false" 
                 id="clientDetails"
                 href="%{clientUrl}" theme="ajax"
                 listenTopics="showDet" formId="detFrm">
          </sx:div>
      </td>              
   </tr>
</table>
<s:hidden name="webClientNo" id="webClientNo" value=""/>
<input type="hidden" name="pListener" onclick="handleReturnedData()"/><br/>
<s:if test="owner and !disableInsuranceConversion">
	<s:if test="quote.quoteStatus=='covered'">
	   <input type="button" 
	          name="convertButton" 
	          id="convertBtn"
	          value="Convert Policy"
	          onclick="doPolicy('policy','')"/> 
	</s:if>
	<s:elseif test="quote.quoteStatus=='new'||quote.quoteStatus=='quote' ||quote.quoteStatus=='complete'">
	   <s:if test="quote.quoteType=='motor' && (quote.coverType=='car' || quote.coverType==null)">
	      <input type="button" 
	          name="convertCarQuoteButton"  
	          id="convertBtn"
	          value="Convert to CAR Quote"
	          onclick="doPolicy('quote','car')"/>
	   </s:if>
	   <s:if test="quote.quoteType=='motor' && (quote.coverType=='tp' || quote.coverType==null)">
	            <input type="button" 
	          name="convertTpQuoteButton"  
	          id="convertBtn"
	          value="Convert to TP Quote"
	          onclick="doPolicy('quote','tp')"/>
	   </s:if>
	   <s:if test="quote.quoteType=='home' && (quote.coverType=='bldg' || quote.coverType==null)">
	            <input type="button" 
	          name="convertBldgQuoteButton"  
	          id="convertBtn"
	          value="Convert to BLDG Quote"
	          onclick="doPolicy('quote','bldg')"/>
	   </s:if>
	   <s:if test="quote.quoteType=='home' && (quote.coverType=='cnts' || quote.coverType==null)">
	            <input type="button" 
	                   name="convertCntsQuoteButton"  
	          		   id="convertBtn"
	                   value="Convert to CNTS Quote"
	                   onclick="doPolicy('quote','cnts')"/>                   
	   </s:if>
	   <s:if test="quote.quoteType=='home' && (quote.coverType=='bldg+cnts' || quote.coverType=='bldg_cnts' || quote.coverType==null)">
	            <input type="button" 
	                   name="convertHomeQuoteButton"  
	          		   id="convertBtn"
	                   value="Convert to Home Quote"
	                   onclick="doPolicy('quote','bldg+cnts')"/>                   
	   </s:if>
	   <s:if test="quote.quoteType=='home' && (quote.coverType=='inv' || quote.coverType==null)">
	            <input type="button" 
	                   name="convertHomeQuoteButton"  
	          		   id="convertBtn"
	                   value="Convert to Investor Quote"
	                   onclick="doPolicy('quote','inv')"/>                   
	   </s:if>
	   
	</s:elseif>
		 
</s:if>

<s:if test="administrator">
	<h3>Disable Quote?</h3>
    <s:select name="reason" list="reason" id="disableReason" label="Reason" headerKey="" headerValue="Please choose..." value="" />
    <input type="button"
                name="commitButton"
                value="Disable"
                onclick="disableQuote('commit')"/>
</s:if>
    
<s:actionerror />
<s:fielderror />
<s:property value="conversionResponse"/>

</s:form>
</body>
</html>
