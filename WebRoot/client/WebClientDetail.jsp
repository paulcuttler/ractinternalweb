<%@ page language="java" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<html>

<head>
    <title>WebClientDetail</title>  
    <s:head/>

</head>

  <body>

    <s:if test="%{webClientNo!=null}">
    
      <h3><s:property value="webClient.title"/>&nbsp;
                         <s:property value="webClient.givenNames"/>&nbsp;
                         <s:property value="webClient.surname"/></h3>
      <table>                 
          <tr><td>Gender</td><td><b><s:property value="webClient.gender"/></b></td>
         <td>Date of Birth</td><td><b><s:property value="webClient.birthDate"/></b></td></tr>
         <tr><td>Home Phone</td><td><b><s:property value="webClient.homePhone"/></b></td>
         <td>Work Phone</td><td><b><s:property value="webClient.workPhone"/></b></td></tr>
         <tr><td>Mobile Phone</td><td><b><s:property value="webClient.mobilePhone"/></b></td>
         <td>Fax</td><td><b><s:property value="webClient.faxNo"/></b></td></tr>
         <tr><td>Email Address</td><td><b><s:property value="webClient.emailAddress"/></b></td></tr>
         <tr><td valign="top">Residential Address</td><td valign="top"><b><s:property value="webClient.resiStreetChar"/>
                                                &nbsp;<s:property value="webClient.resiStreet"/><br/>
                                                <s:property value="webClient.resiSuburb"/></b></td>
          <!--<s:if test="%{RactResiAddress!=null}">
                <td><s:property value="RactResiAddress"/></td>
             </s:if>-->                                 
         </tr>
         <tr><td valign="top">Postal Address</td><td valign="top"><b>
                             <s:if test="webClient.postAddress!='' && webClient.postAddress!=null">
                                  <s:property value="webClient.postAddress"/><br/>
                             </s:if>
                             <s:if test="webClient.postStreet!='' && webClient.postStreet!=null">
                                           <s:property value="webClient.postStreetChar"/>
                                              &nbsp;<s:property value="webClient.postStreet"/><br/>
                             </s:if>  
                                              <s:property value="webClient.postSuburb"/>
                                              &nbsp;<s:property value="webClient.postState"/>
                                              &nbsp;<s:property value="webClient.postCode"/><br/>
                                              <s:property value="webClient.postCountry"/></b></td>
          <!-- <s:if test="%{RactResiAddress!=null}">
               <td><s:property value="RactPostalAddress"/></td>
            </s:if> -->                        
         </tr>
         <tr><td>RACT Client No</td><td><b><s:property value="webClient.ractClientNo"/></b></td></tr>
         
         <s:if test="webClient instanceof com.ract.web.insurance.WebInsClient">        
	         <tr><td>Is a driver?</td><td><b><s:property value="webClient.driver"/></b></td>
	         <td>Is an owner?</td><td><b><s:property value="webClient.owner"/></b></td></tr>        
	         
	         <tr><td>Is preferred address?</td><td><b><s:property value="webClient.preferredAddress"/></b></td>
	         <td>Is a group?</td><td><b><s:property value="webClient.cltGroup"/></b></td></tr>
	         
	         <tr><td>Driving frequency</td><td><b><s:property value="webClient.drivingFrequency"/></b></td>
	         <td>Year commenced driving</td><td><b><s:property value="webClient.yearCommencedDriving"/></b></td></tr>
	         <tr><td>Number of accidents</td><td><b><s:property value="webClient.numberOfAccidents"/></b></td>
	         <td>Current NCB</td><td><b><s:property value="webClient.currentNCD"/></b></td></tr>
	         <s:if test="%{hasPolicies}">
	           <tr><td>Owns Policies</td><td><b><s:property value="policyList"/></b></td></tr>
	         </s:if>
         </s:if>
         
      </table>
      <s:if test="%{webClient.driver}">
        <table>
            <tr>
               <td><b>Type  </b></td>
               <td><b>Month </b></td>
               <td><b>Year  </b></td>
               <td><b>Detail</b></td>
            </tr>
            <s:iterator value="detList" status="cStat">              
               <tr class="<s:if test="#cStat.odd == false">odd</s:if><s:else>even</s:else>">
                  <td><s:property value="detailType"/></td>
                  <td align="right"><s:property value="detMonth"/></td>
                  <td><s:property value="detYear"/></td>
                  <td><s:property value="detail"/></td>
               </tr>
         </s:iterator>
        </table>
      </s:if>
      <s:if test="webClient instanceof com.ract.web.membership.WebMembershipClient || (webClient.owner && !webClient.cltGroup)">
      	<input type="button" name="matchButton" value="Match RACT Client" onclick="matchClient('<s:property value="webClientNo"/>')"/>
	  </s:if>
    </s:if>
  </body>
</html>
