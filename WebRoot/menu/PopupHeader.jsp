<%@ taglib prefix="s" uri="/struts-tags"%>
<%--
Header contains information about the user currently logged into the application.
will contain the security information
--%>

<span style="background:<s:property value="#session['backgroundColour']"/>">
<table width="100%" border="0" cellspacing="0" cellpadding="0" >
  <tr>
    <td><span class="dataValueYellow"><s:property value="#session['systemName']"/></span></td>
    <td><span class="infoTextSmall"><s:property value="#session['userId']"/></span></td>
    <td><span class="infoTextSmall"><s:property value="#session['userName']"/></span></td>
    <td><span class="infoTextSmall"><s:property value="#session['salesBranchCode']"/></span></td>
    <td><span class="infoTextSmall"><s:property value="#session['printerGroup']"/></span></td>
    <td><span class="infoTextSmall">Logged on since <s:property value="#session['sessionStart']"/></span></td>
  </tr>
</table>
</span>

