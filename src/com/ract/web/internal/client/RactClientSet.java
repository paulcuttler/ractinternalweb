package com.ract.web.internal.client;

import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;

import javax.naming.InitialContext;

import org.apache.struts2.interceptor.SessionAware;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ejb3plugin.InjectEJB;
import com.ract.client.Client;
import com.ract.client.ClientEJBHelper;
import com.ract.client.ClientMgr;
import com.ract.client.ClientMgrLocal;
import com.ract.client.ClientVO;
import com.ract.common.BranchVO;
import com.ract.common.ExceptionHelper;
import com.ract.user.User;
import com.ract.user.UserMgrLocal;
import com.ract.util.DateTime;
import com.ract.util.LogUtil;
import com.ract.web.client.CustomerMgrLocal;
import com.ract.web.client.WebClient;
import com.ract.web.convert.client.ClMaster;
import com.ract.web.convert.client.WebClientMgrLocal;
import com.ract.web.insurance.WebInsClient;
import com.ract.web.insurance.WebInsGlMgrRemote;
import com.ract.web.insurance.WebInsMgrLocal;
import com.ract.web.insurance.WebInsMgrRemote;
import com.ract.web.insurance.WebInsQuote;
import com.ract.web.internal.security.Login;
import com.ract.web.membership.WebMembershipClient;
import com.ract.web.membership.WebMembershipMgrLocal;
import com.ract.web.membership.WebMembershipTransaction;
import com.ract.web.membership.WebMembershipTransactionHeader;

public class RactClientSet extends ActionSupport implements SessionAware
{
	private static final String SUBSYSTEM_WEB = "WEB";

	private static final String ACTION_CREATE_CLIENT = "makeNewClient";

	private static final String ACTION_ASSOCIATED_CLIENT = "useSelectedClient";

	private Integer webClientNo;

	private Integer selectedRactClientNo;

	private String process;

	@InjectEJB(name = "CustomerMgr")
	private CustomerMgrLocal customerMgr;

	@InjectEJB(name = "WebInsMgr")
	private WebInsMgrLocal webInsMgr = null;

	@InjectEJB(name = "ClientMgrBean")
	private ClientMgrLocal cltMgr = null;

	@InjectEJB(name = "WebClientMgr")
	private WebClientMgrLocal webClientMgrLocal = null;	
	
	@InjectEJB(name = "WebMembershipMgr")
	private WebMembershipMgrLocal webMemMgr = null;

	private String conversionResponse;

	private Map session;

	@InjectEJB(name="UserMgrBean")	
	private UserMgrLocal userMgrLocal;		
	
	public String execute() throws Exception
	{
	  String userId = (String)session.get(Login.USER_ID);	
	  User user = userMgrLocal.getUser(userId);
	  
		LogUtil.debug(this.getClass(), "\nRactClientSet.execute"
				+ "\nwebClientNo = " + this.webClientNo + "\nselectedRactClientNo = "
				+ this.selectedRactClientNo + "\nprocess = " + this.process);
		String result = "";
		try
		{
			LogUtil.info(this.getClass(), "#-# customerMgr.getWebClient(" + this.webClientNo + ")");
			WebClient clt = customerMgr.getWebClient(webClientNo);
			LogUtil.info(this.getClass(), "#-# customerMgr.getWebClient(" + this.webClientNo + ") COMPLETE");
			
			// check that quote or membership has not been converted
			if (clt instanceof WebInsClient)
			{
				LogUtil.info(this.getClass(), "#-# webInsMgr.getQuote(" + clt + ")");
				WebInsQuote quote = webInsMgr.getQuote(((WebInsClient) clt)
						.getWebQuoteNo());
				LogUtil.info(this.getClass(), "#-# webInsMgr.getQuote(" + clt + ") ");
				
				if (quote.getQuoteStatus().equals(WebInsQuote.CONVERTED))
				{
					conversionResponse = "Unable to make any client changes as the quote has already been converted.";
					return ERROR;
				}
			}
			else
				if (clt instanceof WebMembershipClient)
				{	
					
					LogUtil.info(this.getClass(), "#-#  webMemMgr.getWebMembershipTransactionHeadersByClient(" + clt.getWebClientNo() + ")");
					WebMembershipTransactionHeader webMembershipTransactionHeader = webMemMgr
							.getWebMembershipTransactionHeadersByClient(clt.getWebClientNo());
					LogUtil.info(this.getClass(), "#-# webMemMgr.getWebMembershipTransactionHeadersByClient(" + clt.getWebClientNo() + ") Complete");
					if (webMembershipTransactionHeader != null &&
							WebMembershipTransactionHeader.STATUS_CONVERTED.equals(webMembershipTransactionHeader.getConversionStatus())					
							)
					{
						conversionResponse = "Unable to make any client changes as the membership has already been converted.";
						return ERROR;
					}
				}

			if (process.equalsIgnoreCase(ACTION_ASSOCIATED_CLIENT))
			{
				LogUtil.info(this.getClass(), "#-#  clt.setRactClientNo( " + this.selectedRactClientNo + ")");
				clt.setRactClientNo(this.selectedRactClientNo);
				LogUtil.info(this.getClass(), "#-#   clt.setRactClientNo( " + this.selectedRactClientNo + ") COMPLETE");
				
				LogUtil.info(this.getClass(), "#-#  customerMgr.updateWebClient( " + clt + ")");
				customerMgr.updateWebClient(clt);
				LogUtil.info(this.getClass(), "#-#  customerMgr.updateWebClient( " + clt + ") COMPLETE");
				
				conversionResponse = ACTION_ASSOCIATED_CLIENT;
				return SUCCESS;
			}
			else
				if (process.equalsIgnoreCase(ACTION_CREATE_CLIENT))
				{				
					//does this client already exist?
					LogUtil.info(this.getClass(), "#-#  webClientMgrLocal.getIdenticalClients(" + clt + ")");
					Collection<ClMaster> identicalClients = webClientMgrLocal.getIdenticalClients(clt);
					LogUtil.info(this.getClass(), "#-#  webClientMgrLocal.getIdenticalClients(" + clt + ") COMPLETE");
					
					if (identicalClients.size() > 0)
					{
						String clientList = "";
						int counter = 0;
						for (Iterator<ClMaster> i = identicalClients.iterator();i.hasNext();)
						{
							counter++;
							if (counter > 1)
							{
								clientList += ", ";
							}
							clientList += i.next().getClientNo().toString();
						}
						
						conversionResponse = "Unable to create client as there are exact matches for this client in the RACT client database ("+clientList+")";
						return ERROR;
					}
					
					if (clt.getResiStsubid() == null)
					{
						
						//TODO create a residential street suburb and set on web client						
						
						conversionResponse = "Residential street/suburb does not exist. Client must be created manually.";
						return ERROR;
					}
					else
					{
											
						if (clt.getPostStsubid() == null)
						{
							
							//TODO create a postal street suburb and set on web client
							
							conversionResponse = "Postal street/suburb does not exist. Client must be created manually.";
							return ERROR;
						}
						else
						{
							ClientVO rClt = new ClientVO();
							rClt.setTitle(clt.getTitle());
							rClt.setGivenNames(clt.getGivenNames());
							rClt.setSurname(clt.getSurname());
							rClt.setBirthDate(clt.getBirthDate());
							if (clt.getGender() != null)
							{
								if (clt.getGender().equalsIgnoreCase(ClientVO.GENDER_MALE))
								{
									rClt.setSex(true);
								}
								else
								{
									if (clt.getGender().equalsIgnoreCase(ClientVO.GENDER_FEMALE))
									{
										rClt.setSex(false);
									}
								}
							}
							rClt.setSmsAllowed(true);
							rClt.setHomePhone(clt.getHomePhone());
							rClt.setWorkPhone(clt.getWorkPhone());
							rClt.setMobilePhone(clt.getMobilePhone());
							rClt.setEmailAddress(clt.getEmailAddress());
//							rClt.setFaxNumber(clt.getFaxNo());
							rClt.setResiStreetChar(clt.getResiStreetChar());
							rClt.setResiStsubId(clt.getResiStsubid());
							rClt.setResiLatitude(clt.getResiLatitude());
							rClt.setResiLongitude(clt.getResiLongitude());
							rClt.setResiGnaf(clt.getResiGnaf());
							rClt.setResiAusbar(clt.getResiAusbar());
							try
							{
								Integer.parseInt(clt.getPostProperty());
								rClt.setPostProperty("PO BOX " + clt.getPostProperty());
							}
							catch(Exception ex)
							{
								rClt.setPostProperty(clt.getPostProperty());
							}
							rClt.setPostStreetChar(clt.getPostStreetChar());
							rClt.setPostStsubId(clt.getPostStsubid());
							rClt.setPostDpid(clt.getPostDpid());
							rClt.setPostLatitude(clt.getPostLatitude());
							rClt.setPostLongitude(clt.getPostLongitude());
							rClt.setPostGnaf(clt.getPostGnaf());
							rClt.setPostAusbar(clt.getPostAusbar());
							rClt.setMadeDate(new DateTime());
							rClt.setLastUpdate(new DateTime());
							rClt.setMadeID(userId);
							rClt.setDerivedFields();
							rClt.setMarket(true);

							if (clt instanceof WebMembershipClient)
							{
								rClt.setGroupClient(false);
							}
							else
								if (clt instanceof WebInsClient)
								{
									if (((WebInsClient) clt).isCltGroup())
									{
										rClt.setGroupClient(true);
									}
									else
									{
										rClt.setGroupClient(false);
									}
								}

							rClt.setDerivedFields();
							
							LogUtil.info(this.getClass(), "#-# cltMgr.createClient(--)");
							rClt = cltMgr.createClient(rClt, user.getUserID(), Client.HISTORY_CREATE, user.getSalesBranchCode(),
									"", SUBSYSTEM_WEB);
							LogUtil.info(this.getClass(), "#-#  cltMgr.createClient(--) COMPLETE");
							
							LogUtil.info(this.getClass(), "#-#  clt.setRactClientNo( " + this.selectedRactClientNo + ")");						
							clt.setRactClientNo(rClt.getClientNumber());
							LogUtil.info(this.getClass(), "#-#   clt.setRactClientNo( " + this.selectedRactClientNo + ") COMPLETE");
							
							LogUtil.info(this.getClass(), "#-#  customerMgr.updateWebClient( " + clt + ")");	
							customerMgr.updateWebClient(clt);
							LogUtil.info(this.getClass(), "#-#  customerMgr.updateWebClient( " + clt + ") COMPLETE");	

							this.conversionResponse = ACTION_CREATE_CLIENT;
							
							return SUCCESS;
						}
					}
				}
				else
				{
					this.conversionResponse = "Unknown action "+process;
					return ERROR;
				}
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
			this.conversionResponse = "Error: "+ExceptionHelper.getExceptionStackTrace(ex);
			return ERROR; 
		}
	}

	public Integer getWebClientNo()
	{
		return webClientNo;
	}

	public void setWebClientNo(Integer webClientNo)
	{
		this.webClientNo = webClientNo;
	}

	public Integer getSelectedRactClientNo()
	{
		return selectedRactClientNo;
	}

	public void setSelectedRactClientNo(Integer selectedRactClientNo)
	{
		this.selectedRactClientNo = selectedRactClientNo;
	}

	public String getProcess()
	{
		return process;
	}

	public void setProcess(String process)
	{
		this.process = process;
	}

	public String getConversionResponse()
	{
		return conversionResponse;
	}

	public void setConversionResponse(String conversionResponse)
	{
		this.conversionResponse = conversionResponse;
	}

	@Override
	public void setSession(Map session)
	{
		this.session = session;
	}

	public Map getSession()
	{
		return this.session;
	}

}
