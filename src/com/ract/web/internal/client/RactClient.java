package com.ract.web.internal.client;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ejb3plugin.InjectEJB;
import com.ract.client.Client;
import com.ract.client.ClientEJBHelper;
import com.ract.client.ClientMgr;
import com.ract.client.ClientMgrLocal;
import com.ract.client.ClientVO;
import com.ract.util.LogUtil;
import com.ract.web.client.CustomerMgrLocal;
import com.ract.web.client.CustomerMgrRemote;
import com.ract.web.client.WebClient;
import com.ract.web.convert.client.*;
import com.ract.web.insurance.*;
import java.io.Serializable;
import java.util.*;
import javax.naming.InitialContext;
import org.apache.struts2.interceptor.SessionAware;

public class RactClient extends ActionSupport implements SessionAware
{
   private Integer webClientNo;
   private WebClient webClt;
   private ArrayList<ClientVO> custList;
   
   private Map session = null;
   
   @InjectEJB(name="WebInsMgr")   
   private WebInsMgrLocal webInsMgr = null;
   
   @InjectEJB(name="ClientMgrBean")   
   private ClientMgrLocal clientMgr = null;
   
   @InjectEJB(name="CustomerMgr")
   private CustomerMgrLocal customerMgr = null;
   
   @InjectEJB(name="WebClientMgr")
   private WebClientMgrLocal webClientMgr = null;   
   
   
   public Integer getWebClientNo() {
	  return webClientNo;
   }

   public void setWebClientNo(Integer clientNo) {
	  this.webClientNo = clientNo;
   }
   
   public String execute() throws Exception
   {
	   System.out.println("\n*****************************"
			            + "\nClientNo = " + this.webClientNo
			            + "\n*****************************"  );
//	   webClt = webInsMgr.getWebInsClient(this.webClientNo);
	   
	   LogUtil.info(this.getClass(), "#-# customerMgr.getWebClient(" + this.webClientNo + ")");
	   webClt = customerMgr.getWebClient(this.webClientNo);
	   LogUtil.info(this.getClass(), "#-# customerMgr.getWebClient(" + this.webClientNo + ") COMPLETE");
	   
	   LogUtil.info(this.getClass(), "#-# webClientMgr.getClientList(" + webClt + ")");
	   Collection<ClMaster> list = webClientMgr.getClientList(webClt);
	   LogUtil.info(this.getClass(), "#-# webClientMgr.getClientList(" + webClt + ") COMPLETE");
	   
	   custList = new ArrayList<ClientVO>();
	   //review conversion from clmaster to clientvo	   
	   for(Iterator<ClMaster> i = list.iterator();i.hasNext();)
	   {
		   ClMaster clm = i.next();
		   
		   LogUtil.info(this.getClass(), "#-# clientMgr.getClient(" + clm.getClientNo() + ")");
		   ClientVO cust = clientMgr.getClient(clm.getClientNo());
		   LogUtil.info(this.getClass(), "#-# clientMgr.getClient(" + clm.getClientNo() + ") COMPLETE");
		   
		   custList.add(cust);
	   }
	   return SUCCESS;
   }

	@Override
	public void setSession(Map session) 
	{
		this.session = session;
	}

	public ArrayList<ClientVO> getCustList() {
		return custList;
	}

	public void setCustList(ArrayList<ClientVO> custList) {
		this.custList = custList;
	}

	public WebClient getWebClt() {
		return webClt;
	}

	public void setWebClt(WebClient webClt) {
		this.webClt = webClt;
	}
}
