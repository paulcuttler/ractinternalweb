package com.ract.web.internal.client;

import java.io.Serializable;
import java.lang.reflect.Method;
import java.rmi.RemoteException;

import javax.naming.InitialContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.opensymphony.xwork2.ActionSupport;
import com.ract.client.*;
import com.ract.common.ExceptionHelper;
import com.ract.common.ServiceLocator;
import com.ract.common.integration.RACTPropertiesProvider; 
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ejb3plugin.InjectEJB;
import com.ract.web.insurance.*;
import com.ract.util.*;
import org.json.*;

public class RactClientDetail extends ActionSupport 
{
	private ClientVO customer;
	private Integer selectedRactClientNo;
	private String conversionResponse;
	
  @InjectEJB(name="ClientMgrBean")	
	private ClientMgrLocal clientMgr = null;

	public Integer getSelectedRactClientNo() {
		return selectedRactClientNo;
	}

	public void setSelectedRactClientNo(Integer custNo) {
		this.selectedRactClientNo = custNo;
	}


	public String execute() throws Exception
	{
		System.out.println("\nRactClientDetail.execute"
				+ "\nselectedRactClientNo = " + this.selectedRactClientNo
		);		
		if(selectedRactClientNo!=null)
		{
			customer = clientMgr.getClient(selectedRactClientNo);
			JSONArray ja = new JSONArray();
			addField("rGender",customer.getGender(),ja);         
			if(customer.getBirthDate()==null)
			{
				addField("rDateOfBirth"," ",ja);
			}
			else
			{
				addField("rDateOfBirth",customer.getBirthDate().formatShortDate(),ja);
			}
			addField("rClientNo",customer.getClientNumber().toString(),ja);
			addField("rHomePhone",customer.getHomePhone(),ja);         
			addField("rWorkPhone",customer.getWorkPhone(),ja);         
			addField("rMobilePhone", customer.getMobilePhone(),ja);         
//			addField("rFaxNo", customer.getFaxNumber(),ja);         
			addField("rEmailAddress",customer.getEmailAddress(),ja);         
			addField("rResidentialAddress",customer.getResidentialAddress().getSingleLineAddress(),ja);         
			addField("rPostalAddress",customer.getPostalAddress().getSingleLineAddress(),ja);         
			addField("rClientName",(customer.getTitle().trim() 
					+ " " + customer.getGivenNames().trim() 
					+ " " + customer.getSurname().trim()).trim(),ja);
			conversionResponse = ja.toString(); 	       
		}
		return SUCCESS;
	}

	private void addField(String fieldName, String fieldValue, JSONArray ja)throws Exception
	{
		JSONObject obj = new JSONObject();
		obj.put("fieldName",fieldName);
		if(fieldValue==null || fieldValue.equals(""))
		{
			fieldValue=" - ";
		}	
		obj.put("fieldValue",fieldValue);
		ja.put(obj);
	}

	public String getConversionResponse() {
		return conversionResponse;
	}

	public void setConversionResponse(String conversionResponse) {
		this.conversionResponse = conversionResponse;
	}



}
