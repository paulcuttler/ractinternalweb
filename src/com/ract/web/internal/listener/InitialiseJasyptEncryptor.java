package com.ract.web.internal.listener;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.jasypt.encryption.pbe.StandardPBEStringEncryptor;
import org.jasypt.hibernate3.encryptor.HibernatePBEEncryptorRegistry;

/**
 * This context listener initialises the strong string encryptor provided by Jasypt
 * and registers it in Hibernate's type registry
 * 
 * @see ractInternalWeb web.xml for listener configuration
 * @see ractPublicEJB class com.ract.web.payment.WebPayment for annotated implementation
 * 
 * @author sharpg
 *
 */
public class InitialiseJasyptEncryptor implements ServletContextListener {
	
	static final String JASYPT_HEXADECIMAL_OUTPUT = "hexadecimal"; 

	@Override
	public void contextDestroyed(ServletContextEvent arg0) {
		System.out.println("Jasypt listener destroyed"); // make sure this is logged
	}

	@Override
	public void contextInitialized(ServletContextEvent arg0) {
		System.out.println("Jasypt listener started"); // make sure this is logged
		StandardPBEStringEncryptor ractStringEncryptor = new StandardPBEStringEncryptor();
		ractStringEncryptor.setStringOutputType("hexadecimal");
		ractStringEncryptor.setAlgorithm("PBEWithMD5AndDES"); // ensure STRONG

		// this is REQUIRED
		ractStringEncryptor.setPassword("ENC(CiyzIvJBgEseyCau8jxo6qgpGkZErLPj)");
		
		HibernatePBEEncryptorRegistry registry = HibernatePBEEncryptorRegistry.getInstance();
		registry.registerPBEStringEncryptor("ractStringEncryptor", ractStringEncryptor);
	}
}