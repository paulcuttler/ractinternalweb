package com.ract.web.internal.insurance;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.naming.InitialContext;

import org.apache.struts2.interceptor.SessionAware;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ejb3plugin.InjectEJB;

import com.ract.common.ServiceLocator;
import com.ract.common.integration.RACTPropertiesProvider;
import com.ract.util.LogUtil;
import com.ract.web.insurance.*;
import com.ract.web.convert.insurance.*;

public class QuoteList extends ActionSupport implements SessionAware
{
	private String status;
	private List<QuoteDisp> quoteList;

	private boolean isQuoteNo;
	private String tStatus = null;
	
	@InjectEJB(name="WebInsMgr")
	private WebInsMgrLocal webInsMgr;
	
	@InjectEJB(name="ConvertPolicyMgr")
	private ConvertPolicyMgrLocal convMgr;
	
	private Integer quoteNo;
	private String branch;
	private String branchList;
	private Map<String,Object> session;

	public String execute()throws Exception
    {   
        ArrayList<WebInsQuote>tList = new ArrayList<WebInsQuote>();
        quoteList = new ArrayList<QuoteDisp>();
        String tStatus ;
        String slsbch;
        String allocatedUser;
        String userSalesBranch;
        
        
        this.status=this.status.trim();
    	this.branchList = convMgr.getWebBranchList();
    	userSalesBranch = (String)session.get("salesBranchCode");
    	//if quote no is not empty, check that it is valid
    	if(this.isQuoteNo)  //is a number
     	{
        	WebInsQuote quote = (WebInsQuote) webInsMgr.getQuote(quoteNo);
         	if(quote!=null)
        	{ 
        		    tList.add(quote);
        	}
        	else
        	{
        		addActionError(getText("quoteNo.notFound"));
        		return ERROR;
        	}
        }
    	else if(!userSalesBranch.equalsIgnoreCase("ins")
    			&& this.status.equalsIgnoreCase("ins"))
    	{
    	   addActionError("Only INS users can convert INS quotes");
    	   return ERROR;
    	}
        else 
/* IS a branch or ALL*/        	
        {
        	
        	if(this.status!=null && !this.status.equals(""))
        	{
        	    if(this.status.equalsIgnoreCase("All"))
        	    {
        		   tList = webInsMgr.findQuotesByStatus(WebInsQuote.COVER_ISSUED);
        	    }
        	    else if(branchList.indexOf(this.status.toLowerCase())>-1)
        	    {
                   tList = this.webInsMgr.findQuotesBySalesBranch(this.status.toLowerCase());
        	    }
        	    /* else tList is empty*/
        	}
        }
        for(int xx=0;xx<tList.size();xx++)
        {
        	
        	WebInsQuote quote = tList.get(xx);
            WebInsQuoteDetail aDet = webInsMgr.getDetail(quote.getWebQuoteNo(), 
            		                                     WebInsQuoteDetail.SALES_BRANCH);
            String thisSalesBranch = null;
            if(aDet!=null) 
            {
            	thisSalesBranch = aDet.getFieldValue();
            }
        	if(quote.getQuoteStatus().equals(WebInsQuote.COVER_ISSUED)
        	   && (thisSalesBranch==null || "".equals(thisSalesBranch)))
        	{
        		String agentCode = webInsMgr.getQuoteDetail(quote.getWebQuoteNo(),
                                                            WebInsQuoteDetail.AGENT_CODE);
        		webInsMgr.setQuoteDetail(quote.getWebQuoteNo(),
		                                 WebInsQuoteDetail.SALES_BRANCH, 
		                                 webInsMgr.allocateSalesBranch(agentCode));

        		
        		/*String agentCode = webInsMgr.getQuoteDetail(quote.getWebQuoteNo(),
        				                                    WebInsQuoteDetail.AGENT_CODE);
        		if(agentCode!=null && !agentCode.equals(""))
        		{
        		   String restriction = convMgr.getRestriction(agentCode);
        		   if(restriction != null && restriction.equalsIgnoreCase("ins"))
        		   {
               		   webInsMgr.setQuoteDetail(quote.getWebQuoteNo(),
			                     WebInsQuoteDetail.SALES_BRANCH, 
			                     "ins");
        		   }
        		   else
        		   {
               		   webInsMgr.setQuoteDetail(quote.getWebQuoteNo(),
			                                    WebInsQuoteDetail.SALES_BRANCH, 
			                                    webInsMgr.allocateSalesBranch());
  
        		   }
        		}
        		else
        		{
        		webInsMgr.setQuoteDetail(quote.getWebQuoteNo(),
        				                 WebInsQuoteDetail.SALES_BRANCH, 
        				                 webInsMgr.allocateSalesBranch());
        		}*/
        	}
 
        	QuoteDisp d = new QuoteDisp(quote);
        	quoteList.add(d);
        }
     	return SUCCESS;
    }
    
    public void validate()
    {   	
    	String tQuoteNo = null;
       	if(this.status==null
          	  || this.status.equals(""))
       	{
       	//	addFieldError("status",getText("status.empty"));
       		
       	}
       	else
       	{
       		if(this.status.charAt(0)>='0'
       			&& this.status.charAt(0)<='9')  //is a number
       		{
       			tQuoteNo = this.stripBlanks(this.status);
 /*      			if(tQuoteNo.length()!=8)
       			{
       				addFieldError("status",getText("quoteNo.shortNum"));
       			}*/
       			if(notAllDigits(tQuoteNo))
       			{
       				addFieldError("status",getText("quoteNo.nonNumericalDigits"));
       			}
       			try {
					this.quoteNo = WebInsQuote.validateQuoteNo(tQuoteNo);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					System.out.println("Error validating quote no");
					addFieldError("status","Error validating quote no");
				}
 /*      			String checkDigit = WebInsQuote.checkDigit(tQuoteNo.substring(1), 7);
       			if(!tQuoteNo.startsWith(checkDigit))
       			{
       				addFieldError("status",getText("quoteNo.invalid"));
       			}    	
       			//quote no should be full 8 digit string
       			   this.quoteNo = new Integer(tQuoteNo.substring(1));
*/
       			if(this.quoteNo==null)
       			{
       				addFieldError("status","Invalid quote number");
       			}
       			this.isQuoteNo = true;
       		}
       	/*	else
       		{
       			this.isQuoteNo = false;
       		
       			if(getText("status.options").indexOf(this.status)<0)
       			{
       				addFieldError("status",getText("status.invalid"));
       			}
       		}*/
       	}
    }
    
    private boolean notAllDigits(String inStr)
    {
    	for(int xx=0; xx< inStr.length();xx++)
    	{
    		char ch = inStr.charAt(xx);
    		if(ch<'0' || ch>'9')return true;
    	}
    	return false;
    }
    
    private String quoteDescription(WebInsQuote qt)
    {
    	String desc = null;
    	try
    	{
    		desc = qt.getSecuredQuoteNo()
    	            + " " + qt.getCreateDate()
    	            + " " + qt.getQuoteStatus();
    	}
    	catch(Exception ex)
    	{
    		ex.printStackTrace();
    	}
    	return desc;
    }
    
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public List getQuoteList() {
		return quoteList;
	}

	public void setQuoteList(List list) {
		this.quoteList = list;
	}

	private String translateStatus(String status)
	{
       	String[] options = getText("status.options").split(",");
        String[] sList =   getText("status.values").split(",");
        String tStatus = null;
        String lStatus = status.trim();
         
     	if(status.equalsIgnoreCase("all"))
    	{
    		tStatus = "All";
    	}
    	else
    	{
    		tStatus = "";
            for(int xx=0;xx<options.length;xx++)
    		{
    			if(options[xx].equalsIgnoreCase(lStatus))
    			{
    				tStatus = sList[xx];
    			}
    		}
    	}
	    return tStatus;
	}
	
	private String stripBlanks(String in)
	{
		String out="";
		for(int xx=0;xx<in.length();xx++)
		{
			char ch = in.charAt(xx);
			if(ch != ' ')
			{
				out += ch;
			}
		}
		return out;
	}
	
	private class QuoteDisp extends WebInsQuote implements Serializable{
       public String securedQuoteNo;
       public String allocatedUser;
       public String allocatedBranch;
       
       public QuoteDisp(WebInsQuote qt)throws Exception
       {
    	   
    	   this.setCoverType(qt.getCoverType());
    	   this.setCreateDate(qt.getCreateDate());
    	   this.setQuoteDate(qt.getQuoteDate());
    	   this.setQuoteStatus(qt.getQuoteStatus());
    	   this.setQuoteType(qt.getQuoteType());
    	   this.setStartCover(qt.getStartCover());
    	   this.setWebQuoteNo(qt.getWebQuoteNo());
    	   this.securedQuoteNo = qt.getSecuredQuoteNo();
           this.allocatedBranch = webInsMgr.getQuoteDetail(qt.getWebQuoteNo(), WebInsQuoteDetail.SALES_BRANCH);
           this.allocatedUser = webInsMgr.getQuoteDetail(qt.getWebQuoteNo(), "allocatedUser");

       }
 	}

	@Override
	public void setSession(Map<String, Object> session)
	{
		this.session = session;
	}
	
}
