package com.ract.web.internal.insurance;

import java.io.Serializable;
import java.lang.reflect.Method;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;

import javax.naming.InitialContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.opensymphony.xwork2.ActionSupport;
import com.ract.client.ClientMgrLocal;
import com.ract.client.ClientVO;
import com.ract.common.ExceptionHelper;
import com.ract.common.ServiceLocator;
import com.ract.common.integration.RACTPropertiesProvider;
import com.ract.insurance.Policy;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ejb3plugin.InjectEJB;
import com.ract.web.client.CustomerMgrLocal;
import com.ract.web.client.CustomerMgrRemote;
import com.ract.web.client.WebClient;
import com.ract.web.insurance.*;
import com.ract.web.membership.WebMembershipClient;
import com.ract.common.*;

public class WebClientDetail extends ActionSupport
{

	private Integer webClientNo;
	private Boolean hasPolicies;
	private String policyList = "";
	private WebClient webClient;
	private ArrayList<WebInsClientDetail> detList;
	@InjectEJB(name="WebInsMgr")
	private WebInsMgrLocal webInsMgr;
	@InjectEJB(name="CustomerMgr")
	private CustomerMgrLocal customerMgr;
	@InjectEJB(name="CommonMgrBean")
	private CommonMgrLocal commonMgr;
	@InjectEJB(name="ClientMgrBean")
	private ClientMgrLocal cltMgr;
	private String clientType;
	private String RactPostalAddress;
	private String RactResiAddress;
	

	public String execute() throws Exception
	{
		StreetSuburbVO rs;
		if (this.webClientNo == null)
			return SUCCESS;
		
	/*	System.out.println("webClientNo="+webClientNo);*/
		
		this.webClient = customerMgr.getWebClient(this.webClientNo);
		if(webClient.getResiStsubid()!=null)
		{
			rs = commonMgr.getStreetSuburb(webClient.getResiStsubid());
			webClient.setResiStreet(rs.getStreet());
			webClient.setResiSuburb(rs.getSuburb());
		}
		if(webClient.getPostStsubid()!=null)
		{
		    rs = commonMgr.getStreetSuburb(webClient.getPostStsubid());
		    if(webClient.getPostProperty()!=null
		       && !webClient.getPostProperty().trim().equals(""))
		    {
		       String poBox = "";	
		       try
		       {
		    	   new Integer(webClient.getPostProperty());
		    	   poBox = "PO Box ";
		       }
		       catch(Exception ei)
		       { 
		    	 //do nothing  
		       }
		       webClient.setPostAddress(poBox + webClient.getPostProperty());
		    }
		    webClient.setPostStreet(rs.getStreet());
		    webClient.setPostSuburb(rs.getSuburb());
		}
		if (webClient instanceof WebInsClient)
		{
			this.detList = translateList(webInsMgr.getClientDetails(this.webClientNo));
			this.hasPolicies = new Boolean(false);
			if (webClient.getRactClientNo() != null)
			{
			/*	Collection<Policy> polList = webInsMgr.getClientPolicies(webClient.getRactClientNo());
				if (polList.size() > 0)
				{
					Iterator<Policy> it = polList.iterator();
					while (it.hasNext())
					{
						Policy pol = it.next();
						if(pol.getPolicyStatus().equalsIgnoreCase("current")
						  || pol.getPolicyStatus().equalsIgnoreCase("renewal")
						// || pol.getPolicyStatus().equalsIgnoreCase("lapsed")
								)
						{
						   if (!this.policyList.equals(""))
							   this.policyList += ",";
						   this.policyList += pol.getPolicyNumber();
						}
					}

				}*/					
				this.policyList = QuoteAllDetails.getCurrentPolicies(webClient.getRactClientNo());
				if (policyList!=null) hasPolicies = new Boolean(true);

/*compare address with that in the client database */
				ClientVO clt = cltMgr.getClient(webClient.getRactClientNo());
				ResidentialAddressVO ra = clt.getResidentialAddress();
				this.RactResiAddress = ra.getSingleLineAddress();
				PostalAddressVO pa = clt.getPostalAddress();
				this.RactPostalAddress = pa.getSingleLineAddress();
			}
		}
		return SUCCESS;
	}
	
	public WebClient getWebClient()
	{
		return webClient;
	}

	public void setWebClient(WebClient webClient)
	{
		this.webClient = webClient;
	}

	public String getClientType()
	{
		return clientType;
	}

	public void setClientType(String clientType)
	{
		this.clientType = clientType;
	}


	public Integer getWebClientNo()
	{
		return webClientNo;
	}

	public void setWebClientNo(Integer clientNo)
	{
		this.webClientNo = clientNo;
	}

	public ArrayList<WebInsClientDetail> getDetList()
	{
		return detList;
	}

	public void setDetList(ArrayList<WebInsClientDetail> detList)
	{
		this.detList = detList;
	}

	private ArrayList<WebInsClientDetail> translateList(
			ArrayList<WebInsClientDetail> list)
	{
		ArrayList<WebInsClientDetail> outList = new ArrayList<WebInsClientDetail>();
		WebInsClientDetail oldDet = null;
		WebInsClientDetail newDet = null;
		for (int xx = 0; xx < list.size(); xx++)
		{
			oldDet = list.get(xx);
			newDet = new WebInsClientDetail();
			newDet.setDetail(oldDet.getDetail());
			newDet.setDetailType(getText("detType." + oldDet.getDetailType()));
			newDet.setDetMonth(oldDet.getDetMonth());
			newDet.setDetYear(oldDet.getDetYear());
			outList.add(newDet);
		}
		if (outList.size() > 1)
		{
			Collections.sort(outList, new CompareDetail());
		}
		return outList;
	}

	private class CompareDetail implements
			java.util.Comparator<WebInsClientDetail>
	{
		public int compare(WebInsClientDetail d1, WebInsClientDetail d2)
		{
			return d1.compareTo((Object) d2);
		}
	}

	public Boolean getHasPolicies()
	{
		return hasPolicies;
	}

	public void setHasPolicies(Boolean hasPolicies)
	{
		this.hasPolicies = hasPolicies;
	}

	public String getPolicyList()
	{
		return policyList;
	}

	public void setPolicyList(String policyList)
	{
		this.policyList = policyList;
	}

	public String getRactPostalAddress() {
		return RactPostalAddress;
	}

	public void setRactPostalAddress(String ractPostalAddress) {
		RactPostalAddress = ractPostalAddress;
	}

	public String getRactResiAddress() {
		return RactResiAddress;
	}

	public void setRactResiAddess(String ractResiAddess) {
		RactResiAddress = ractResiAddess;
	}


}
