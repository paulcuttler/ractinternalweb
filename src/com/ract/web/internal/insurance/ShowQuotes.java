package com.ract.web.internal.insurance;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.naming.InitialContext;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ejb3plugin.InjectEJB;

import com.ract.common.ServiceLocator;
import com.ract.common.integration.RACTPropertiesProvider;
import com.ract.web.insurance.*;
import com.ract.web.convert.insurance.*;

public class ShowQuotes extends ActionSupport
{
   String branchList;
   String fBranchList;
   
   @InjectEJB(name="ConvertPolicyMgr")
   private ConvertPolicyMgrLocal convMgr;

   public String execute()throws Exception
   {
	   
System.out.println("\n*****************************"
		       + "\n ShowQuotes.execute......");	   
//	  if(convMgr == null)
//	  {
//		  this.connect();
//	  }
   	  this.branchList = convMgr.getWebBranchList();
   	  
   	  System.out.println("\nbranchList = " + branchList
   			          + "\n****************************");
 	  return SUCCESS;
   }
//	private void connect()throws Exception
//	{
//		Properties properties = new Properties();
//		properties.put("java.naming.factory.initial",
//		"org.jnp.interfaces.NamingContextFactory");
//		properties.put("java.naming.factory.url.pkgs",
//		"=org.jboss.naming:org.jnp.interfaces");
//		String jnp = com.ract.util.FileUtil.getProperty("master","java.naming.provider.url");
//		properties.put("java.naming.provider.url", jnp);
//
//		InitialContext ctx = new InitialContext(properties);
//		//bean = (WebInsMgrRemote)ctx.lookup("WebInsMgr/remote");
//		this.convMgr = (ConvertPolicyMgrRemote)ctx.lookup("ConvertPolicyMgr/remote");
//	}

}
