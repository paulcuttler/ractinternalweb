package com.ract.web.internal.insurance;

import java.util.Map;
import java.util.Properties;

import javax.naming.InitialContext;

import org.apache.struts2.interceptor.SessionAware;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ejb3plugin.InjectEJB;
import com.ract.web.insurance.WebInsGlMgrRemote;
import com.ract.web.insurance.WebInsMgr;
import com.ract.web.insurance.WebInsMgrLocal;
import com.ract.web.insurance.WebInsMgrRemote;
import com.ract.web.insurance.WebInsQuote;
import com.ract.web.insurance.WebInsQuoteDetail;
import com.ract.web.convert.insurance.*;

public class ConvertPolicy extends ActionSupport implements SessionAware
{
	
	public Map getSession()
	{
		return session;
	}

	@InjectEJB(name="ConvertPolicyMgr")
	private ConvertPolicyMgrLocal bean;
	
	@InjectEJB(name="WebInsMgr")
	private WebInsMgrLocal webMgr;
	
    private Integer webQuoteNo;
	private String conversionResponse;
	private String convType;
	private String riskType;
	private String printProposal;

	private Map session;
		
	public String getConversionResponse() {
		return conversionResponse;
	}

	public void setConversionResponse(String conversionResponse) {
		this.conversionResponse = conversionResponse;
	}

	public Integer getWebQuoteNo() {
		return webQuoteNo;
	}

	public void setWebQuoteNo(Integer quoteNo) {
		this.webQuoteNo = quoteNo;
	}

	public String execute() throws Exception
	{
		String userId="";
		try
		{
			userId = (String)session.get("userId");
			/*convertType = (String)session.get("convType");*/

System.out.println("\n______________________________"
		         + "\nConvertPolicy.execute"
		         + "\nuserId = " + userId
		         + "\nconvType = " + convType
		         + "\nwebQuoteNo = " + webQuoteNo
		         + "\nprintProposal = " + printProposal
		         + "\n------------------------------");

			WebInsQuote qt = webMgr.getQuote(webQuoteNo);
			if(qt.getQuoteStatus().equals(WebInsQuote.CONVERTED))
			{
				this.conversionResponse = "Already converted to policy";
				return SUCCESS;
			}
			else if(qt.getQuoteStatus().equals(WebInsQuote.QUOTE_CONVERTED))
			{
				this.conversionResponse = "Already converted to Quote";
				return SUCCESS;
			}
			
			if(convType.equals("policy"))
			{
			   if(webMgr.getQuoteDetail(webQuoteNo, WebInsQuoteDetail.SALES_BRANCH)==null)
			   {
				   String salesBranch = webMgr.allocateSalesBranch();
				   if (salesBranch != null) 
				   {
					   webMgr.setQuoteDetail(webQuoteNo, 
						                 WebInsQuoteDetail.SALES_BRANCH, 
						                 salesBranch);
				   }
			   }
			   webMgr.setQuoteDetail(webQuoteNo,
		                             "printProposal",
		                             printProposal);

			   this.conversionResponse = bean.convertPolicy(webQuoteNo,
					                                        userId);
			}
			else if(convType.equals("quote"))
			{
				this.conversionResponse = bean.convertQuote(webQuoteNo,riskType,userId);
			}
			if(!conversionResponse.equalsIgnoreCase("success"))
			{
				return SUCCESS;
			}
		}
		catch(Exception ex)
		{
			this.conversionResponse="Error retrieving data: \n" + ex;
			ex.printStackTrace();
			return ERROR;
		}
		return SUCCESS;
	}

	@Override
	public void setSession(Map session) 
	{
		this.session = session;
	}

	public String getConvType() {
		return convType;
	}

	public void setConvType(String convType) {
		this.convType = convType;
	}

	public String getRiskType() {
		return riskType;
	}

	public void setRiskType(String riskType) {
		this.riskType = riskType;
	}
	
	public String getPrintProposal()
	{
		return this.printProposal;
	}
	public void setPrintProposal(String printProposal)
	{
		this.printProposal = printProposal;
	}
}
