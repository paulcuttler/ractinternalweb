package com.ract.web.internal.insurance;

import java.util.Map;

import org.apache.struts2.interceptor.SessionAware;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ejb3plugin.InjectEJB;
import com.ract.web.convert.insurance.ConvertPolicyMgr;
import com.ract.web.convert.insurance.ConvertPolicyMgrLocal;
import com.ract.web.insurance.WebInsMgrLocal;
import com.ract.web.insurance.WebInsQuote;
import com.ract.web.insurance.WebInsQuoteDetail;

public class DisableQuote extends ActionSupport implements SessionAware {

	private Map<String,Object> session;
  private Integer webQuoteNo;
	private String conversionResponse;
	private String reason;
		
	@InjectEJB(name="WebInsMgr")
	private WebInsMgrLocal bean;
	
	@InjectEJB(name="ConvertPolicyMgr")
	private ConvertPolicyMgrLocal convertPolicyMgr;
	
	@Override
	public void validate() {
		String userSalesBranch = (String) session.get("salesBranchCode");	
	  if (!userSalesBranch.equalsIgnoreCase("INS")) {
			this.setConversionResponse("Only INS users can disable quotes");
			this.addFieldError("webQuoteNo", this.getConversionResponse());	  	
	  }
	   
		if (this.getReason() == null || this.getReason().isEmpty()) {
			this.setConversionResponse("Please provide a reason");
			this.addFieldError("reason", this.getConversionResponse());
		}
		
		if (this.getWebQuoteNo() == null) {
			this.setConversionResponse("Please provide a quote number");
			this.addFieldError("webQuoteNo", this.getConversionResponse());
		} else {
			
			WebInsQuote qt = bean.getQuote(this.getWebQuoteNo());
			if (qt == null) {
				this.setConversionResponse("Unable to find quote for quote number: " + this.getWebQuoteNo());
				this.addFieldError("webQuoteNo", this.getConversionResponse());
			}
		}
	}
	
	@Override
	public String execute() throws Exception {		
		
		String accountNo = bean.getQuoteDetail(this.getWebQuoteNo(), WebInsQuoteDetail.DD_NO);
		if (accountNo != null && !accountNo.isEmpty()) {        	 
	     accountNo = convertPolicyMgr.obfuscate(accountNo);
	     bean.setQuoteDetail(this.getWebQuoteNo(), WebInsQuoteDetail.DD_NO, accountNo.toString());		  
    }
		
		bean.setQuoteStatus(this.getWebQuoteNo(), this.getReason());
		this.setConversionResponse("Quote disabled.");
		
		return SUCCESS;
	}
	
	public Map<String,Object> getSession() {
		return session;
	}
	
	@Override
	public void setSession(Map<String,Object> session) {
		this.session = session;
	}

	public Integer getWebQuoteNo() {
		return webQuoteNo;
	}

	public void setWebQuoteNo(Integer quoteNo) {
		this.webQuoteNo = quoteNo;
	}

	public String getConversionResponse() {
		return conversionResponse;
	}

	public void setConversionResponse(String conversionResponse) {
		this.conversionResponse = conversionResponse;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}
}
