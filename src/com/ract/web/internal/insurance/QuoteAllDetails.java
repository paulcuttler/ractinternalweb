package com.ract.web.internal.insurance;

import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.struts2.interceptor.SessionAware;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ejb3plugin.InjectEJB;
import com.ract.client.ClientMgr;
import com.ract.client.ClientVO;
import com.ract.common.CommonMgrLocal;
import com.ract.common.StreetSuburbVO;
import com.ract.util.DateTime;
import com.ract.util.FileUtil;
import com.ract.web.client.WebClient;
import com.ract.web.convert.insurance.ConvertPolicyMgrLocal;
import com.ract.web.insurance.GlVehicle;
import com.ract.web.insurance.InRfDet;
import com.ract.web.insurance.InRiskSi;
import com.ract.web.insurance.InRiskSiLineItem;
import com.ract.web.insurance.WebInsClient;
import com.ract.web.insurance.WebInsGlMgrLocal;
import com.ract.web.insurance.WebInsMgrLocal;
import com.ract.web.insurance.WebInsQuote;
import com.ract.web.insurance.WebInsQuoteDetail;

public class QuoteAllDetails extends ActionSupport implements SessionAware{
    private static final long serialVersionUID = 2513363887531651267L;
    private Integer quoteNo;
	private ArrayList<DetailDisplay> quoteDetails=null;
	private ArrayList<WebInsClient> clientList=null;
	private Map<String,String> reason = new HashMap<String,String>();
	private WebInsQuote quote;
	private boolean owner = false;	
	private String userId;
	private boolean administrator = false;
	private boolean disableInsuranceConversion = false;
	
	@InjectEJB(name="WebInsMgr")
	private WebInsMgrLocal bean;
	@InjectEJB(name="WebInsGlMgr")
	private WebInsGlMgrLocal glBean;
	@InjectEJB(name="CommonMgrBean")
	private CommonMgrLocal commonMgrBean;
	@InjectEJB(name="ConvertPolicyMgr")
	private ConvertPolicyMgrLocal convMgr;
	@InjectEJB(name="ClientMgrBean")
	private ClientMgr cltMgr;

	private Map<String,Object> session;	
	
  public final static String INSURANCE_SALES_BRANCH = "INS";
	
    public String execute()throws Exception
    { 
	   userId = (String)session.get("userId");
	   String userSalesBranch = (String)session.get("salesBranchCode");	
	   this.setAdministrator(userSalesBranch.equalsIgnoreCase(INSURANCE_SALES_BRANCH));
	   
	   try {
	     this.disableInsuranceConversion = Boolean.parseBoolean(FileUtil.getProperty("master", "disableInsuranceConversion"));
	   } catch (Exception e) {}
			   
	   String quoteSalesBranch = bean.getQuoteDetail(quoteNo, 
                                                     WebInsQuoteDetail.SALES_BRANCH); 
      
       this.quote = bean.getQuote(quoteNo);
       String allocatedUser = bean.getQuoteDetail(quoteNo, "allocatedUser");
       if(allocatedUser==null)
       {
    	   if(quoteSalesBranch!=null
    		  && (quoteSalesBranch.equalsIgnoreCase(INSURANCE_SALES_BRANCH) && !userSalesBranch.equalsIgnoreCase(INSURANCE_SALES_BRANCH)
    		      || !quoteSalesBranch.equalsIgnoreCase(INSURANCE_SALES_BRANCH) && userSalesBranch.equalsIgnoreCase(INSURANCE_SALES_BRANCH)))
    	   {
    		   this.owner = false;
    	   }
    	   else
    	   {
    	   	   bean.setQuoteDetail(quoteNo,"allocatedUser",this.userId);
    	       this.owner = true;
    	   }    
       }
       else if(allocatedUser.equalsIgnoreCase(this.userId))
       {
    	   this.owner = true;
       }
       else 
       {
//		   addActionError("This policy has already been allocated to " + allocatedUser);
//    	   return ERROR;
    	   owner = false;
       }
       
       List<InRfDet> reasonOptions = bean.getLookupData("", "", "NoConvert", new DateTime());
       for (InRfDet inRfDet : reasonOptions) {
      	 reason.put(inRfDet.getrClass(), inRfDet.getrDescription());
       }
       
       String coverType = quote.getCoverType();
       
       this.quoteDetails = this.translateDetail(bean.getAllQuoteDetails(quoteNo), coverType); 
       addSpecifiedItems(this.quoteDetails,quoteNo);
       try
       {
	       this.clientList = bean.getClients(quoteNo);
	       String tData = "";
	       if(clientList!=null)
	       {
		       for(int xx=0;xx<this.clientList.size();xx++)
		       {
		    	   WebInsClient clt = this.clientList.get(xx);
		    	   tData = "";
		    	   if(clt.getRactClientNo()!=null)
		    	   {
		    		  if(QuoteAllDetails.getCurrentPolicies(clt.getRactClientNo())!=null) 
		    		  {
		    			  tData = "Has Policies";
		    		  }
		    		  tData += addressChanged(clt);
		    	   }
		    	   if(!tData.equals(""))clt.setTData(tData);
		       }
	       }
	       addDiscount(quoteNo);
	   }
       catch(Exception ex)
       {
    	   ex.printStackTrace();
    	   return SUCCESS;
       }
       return SUCCESS;
    }
    
    private void addDiscount(Integer quoteNo)throws Exception
    {
	       String fiftyPlus = bean.getQuoteDetail(quoteNo,WebInsQuoteDetail.FIFTY_PLUS);
	       String prodType;
	       if(fiftyPlus!= null && fiftyPlus.equals(WebInsQuoteDetail.TRUE)) prodType = "50+";
	       else prodType = "PIP";
	       String cardNo = bean.getQuoteDetail(quoteNo,"memberCard");
	       BigDecimal discount = null;
	       if(cardNo != null)
	       {
	           ClientVO clt = cltMgr.getClientByMembershipCardNumber(unformatCardNo(cardNo));

		       discount = this.convMgr.getEligibleDiscount(clt.getClientNumber(),
		    	                                           prodType,
		    	                                           quote.getCoverType(),
		    	                                           quote.getStartCover());
		       if(discount!=null && discount.floatValue()!=0)
		       {
		    	   DetailDisplay dd = new DetailDisplay(quoteNo,
		    			                                "PIP Discount",
		    			                                discount + "%",
		    			                                500);
		    	   this.quoteDetails.add(dd);
		       }
	       }
    }
    
    private String unformatCardNo(String cardNo)
    {
    	String oString = "";
    	for(int xx = 0; xx<cardNo.length();xx++)
    	{
    		if(cardNo.charAt(xx)!=',')
    		{
    			oString = oString + cardNo.charAt(xx);
    		}
    	}    	
    	return oString;
    }
/**
 * translateDetail
 * Uses entries in QuoteAllDetails.properties to set display information
 * relevant to each stored detail
 * The properties file is constructed as follows:
 * Property name = field name as stored in WebInsQuoteDetail.fieldName
 * Property value is a comma deliminated list with 4 fields
 * Field 0 = display order
 * Field 1 = Unit ($,% etc)
 * Field 2 = method to be used to process the data (where required)
 * Field 3 = Display heading.
 * @param inList
 * @return
 * @throws Exception
 */
    private ArrayList<DetailDisplay> translateDetail(ArrayList<WebInsQuoteDetail> inList, String coverType) throws Exception {
        ArrayList<DetailDisplay> newList = new ArrayList<DetailDisplay>();
        ArrayList<DetailDisplay> optionList = new ArrayList<DetailDisplay>();

        if (inList != null) {
            String fName;
            String newFieldName;
            String prodType;

            String process;
            String newValue;
            String tText = "";
            String[] aText;
            int displayOrder = 0;
            DetailDisplay newDet = null;
            int maxDisplayOrder = 0;

            prodType = bean.getQuoteDetail(quoteNo, WebInsQuoteDetail.FIFTY_PLUS);
            if (prodType.equals(WebInsQuoteDetail.TRUE)) {
                prodType = "50+";
            } else {
                prodType = "PIP";
            }

            for (WebInsQuoteDetail det : inList) {
                tText = "";
                newValue = "";
                fName = det.getFieldName().trim();
                /* displayOrder = 0; */
                try {
                    
                    if (det.getInKey() != null && det.getInKey() != 0 || det.getFieldName().equalsIgnoreCase("CNTS_PersonalEffects")) {
                        displayOrder = showSelection(det, newList, optionList, prodType, coverType);
                    } else if (det.getStringKey() != null && !det.getStringKey().equals("")) {
                        /* should be gn-table lookups */
                        displayOrder = showGnTableDescription(det, newList, displayOrder, prodType, coverType);
                    } else {
                        tText = getText(fName);
                        aText = tText.split(",");
                        if (aText.length == 4) {
                            newFieldName = aText[3];
                            process = aText[2];
                            displayOrder = Integer.parseInt(aText[0]);
                            if (!process.equals("")) {
                                specialProcess(process, det.getFieldValue(), newList, displayOrder);
                            } else {
                                newValue = det.getFieldValue();
                                String unit = aText[1];
                                if (!unit.equals("")) {
                                    if (unit.equals("$"))
                                        newValue = unit + newValue;
                                    else
                                        newValue += unit;
                                }
                                newDet = new DetailDisplay(det.getWebQuoteNo(), newFieldName, newValue, displayOrder);
                                newList.add(newDet);
                            }
                        } else {
                            newFieldName = fName;
                        }
                    }
                    if (displayOrder > maxDisplayOrder)
                        maxDisplayOrder = displayOrder;
                } catch (Exception ex) {
                    ex.printStackTrace();
                    throw new Exception(">>----> Error handling " + tText + " for field " + fName + "\n", ex);
                }
    		}

            /* re number options */
            if (optionList.size() > 0) {
                maxDisplayOrder++;
                newDet = new DetailDisplay(this.quoteNo, "OPTIONS:", "", maxDisplayOrder++);
                newList.add(newDet);
                for (int xx = 0; xx < optionList.size(); xx++) {
                    newDet = optionList.get(xx);
                    newDet.setDisplaySeq(maxDisplayOrder++);
                    newList.add(newDet);
                }

            }
        }
        Collections.sort(newList);
        return newList;
    }
    
    private void specialProcess(String processName,
    		                    String fieldValue,
    		                    ArrayList<DetailDisplay> list,
    		                    int displayOrder)throws Exception
    {
    	    Class classTarget = this.getClass();
    	    String methodName = processName;
    	    Method methodTarget;
    	    try
    	    {
    	      Class[] parameterTypes = new Class[]{String.class,
    	    		                               ArrayList.class,
    	    		                               Integer.class};
    	      methodTarget = classTarget.getMethod(methodName, parameterTypes);
    	      Object[] arguments = new Object[]{fieldValue,list,new Integer(displayOrder)};
    	      methodTarget.invoke(this, arguments);
    	    }
    	    catch(NoSuchMethodException nsme)
    	    {
    	    	nsme.printStackTrace();
    	    }
    }
    
    
    /**
     * Use lookup tables to provide actual value.
     * @param fName
     * @param fValue
     * @param list
     * @param dipslayOrder
     */
    public int showSelection(WebInsQuoteDetail wDet,
    		                  ArrayList<DetailDisplay> list,
    		                  ArrayList<DetailDisplay> optionList,
    		                  String prodType,
    		                  String coverType)
    {
       String fieldName = "";
       InRfDet det = null;
       DetailDisplay dd = null;
       StringBuffer typeBuffer = new StringBuffer("");
       StringBuffer subTypeBuffer = new StringBuffer("");
       String vValue = "";
       String tText = null;
       String[] tList = null;
       int displayOrder = 0;
       try
       {
    	   /* need to create a lookup in the insurance system
    	    * which can return the correct detail without knowing
    	    * which type of think it is - use only the field name.
    	    */
          if(wDet.getFieldName().equalsIgnoreCase("CNTS_PersonalEffects"))
          {
        	 typeBuffer.append("OPTIONS");
        	 det = new InRfDet();
        	 det.setrDescription("Personal Effects");
          }
          else
          {
        	  try
        	  {
	    	     det = convMgr.getLookupDescription(prodType,
	    	             coverType,
	    	             wDet.getFieldName(),
                         wDet.getFieldValue());
        	  }
        	  catch(Exception ex)
        	  {
        		  det = null;
        	  }
          }
   		  if(typeBuffer.toString().equalsIgnoreCase("FIELDRATINGS")) 
   		  {
   			  // get field name from properties table using type name
   			  fieldName = getText(subTypeBuffer.toString());
   			  vValue = det.getrDescription();
   			  try
   			  {
   				  tText = getText(fieldName.toLowerCase());
   				  tList = tText.split(",");
   				  fieldName = tList[3];
   				  displayOrder = Integer.parseInt(tList[0]);
   			  }
   			  catch(Exception ex)
   			  {
   				  //do nothing
   			  }
   			  
			  dd = new DetailDisplay(wDet.getWebQuoteNo(),
	                     fieldName,
			             vValue,
			             displayOrder);
			  dd.setInKey(det.getRfDateSeq());
			  dd.setStringKey(det.getrKey3());
			  list.add(dd);
   		  }
   		  else if(typeBuffer.toString().equalsIgnoreCase("OPTIONS"))
   		  {
   			  displayOrder = 0;
   			  fieldName = det.getrDescription();
   			  vValue = wDet.getFieldValue();
			  dd = new DetailDisplay(wDet.getWebQuoteNo(),
				                     "-  " + fieldName,
						             vValue,
						             displayOrder);
	          dd.setInKey(det.getRfDateSeq());
	          dd.setStringKey(det.getrKey3());
	          optionList.add(dd);
          }
       }
       catch(Exception ex)
       {
    	   ex.printStackTrace();
       }
    	return displayOrder;
    }
    
    public int showGnTableDescription(WebInsQuoteDetail wDet, 
    		                           ArrayList<DetailDisplay> list,
    		                           int displayOrder,
    		                           String prodType,
    		                           String coverType)throws Exception
    {
        displayOrder++;
    	InRfDet det = null;
        DetailDisplay dd = null;
        String tText = getText(wDet.getFieldName().toLowerCase());
        
        displayOrder = 5000;
        String[] tList = tText.split(",");
        if(tList.length == 4) {
            displayOrder = new Integer(tList[0]);
        }

        String vValue = "";
        try
        {
            det = convMgr.getLookupDescription(prodType, coverType, wDet.getFieldName(), wDet.getFieldValue());
        }
        catch(Exception ex)
        {
        	ex.printStackTrace();
        	throw ex;
        }
		vValue = det.getrDescription(); 
    	dd = new DetailDisplay(wDet.getWebQuoteNo(),
    	                       wDet.getFieldName(),
		                       vValue,
		                       displayOrder);
        dd.setInKey(det.getRfDateSeq());
        dd.setStringKey(det.getrKey3());
        list.add(dd);
        return displayOrder;
    }
    		                	  
    
    public void getStreetSuburb(String stsubid, 
    		                    ArrayList<WebInsQuoteDetail> list,
    		                    Integer displayOrder)throws Exception
    {
    	String gString = null;
    	if(quote.getQuoteType().equalsIgnoreCase("motor"))
    	{
	    	StreetSuburbVO garagedAt = commonMgrBean.getStreetSuburb(new Integer(stsubid));
	    	gString = garagedAt.getStreet() + " " + garagedAt.getSuburb();
	    	if(!garagedAt.getState().equalsIgnoreCase("TAS"))
	    	{
	    		gString += " " + garagedAt.getState();
	    	}
	    	list.add(new DetailDisplay(this.quoteNo,
	    			                       "Garaged Address",
	    			                       gString,
	    			                       displayOrder.intValue()));
    	}
    	else
    	{
//    	   WebInsQuoteDetail det = bean.getDetail(quote.getWebQuoteNo(),"situationStreetNo");
//    	   if(det!=null)gString = det.getFieldValue();
//    	   det = bean.getDetail(quote.getWebQuoteNo(),"situationStreetName");
//    	   if(gString!=null)gString +=" ";
//    	   if(det!=null)gString += det.getFieldValue();
//    	   det = bean.getDetail(quote.getWebQuoteNo(),"situationSuburb");
//    	   if(det!=null) gString += "<br>" + det.getFieldValue();
//    	   list.add(new DetailDisplay(quote.getWebQuoteNo(),
//    			                      "Address",
//    			                      gString,
//    			                      displayOrder.intValue()));
    	}
    }
    
    public void getVehicleDescription(String nvic, ArrayList<WebInsQuoteDetail> list, Integer displayOrder)throws Exception
    {
			GlVehicle veh = glBean.getVehicle(nvic,quote.getQuoteDate());
			int order = displayOrder.intValue();
            WebInsQuoteDetail det = null;
            det = new DetailDisplay(this.quoteNo,
            		                    "Vehicle",
            		                    veh.getVehYear() + " " + veh.getMake() + " " + veh.getModel(),
            		                    order);
            list.add(det);
            det = new DetailDisplay(this.quoteNo,
            		                    "",
            		                    veh.getVariant() + " " + veh.getBodyType(),
            		                    order + 1);
            list.add(det);
            det = new DetailDisplay(this.quoteNo,
            		                    "",
            		                    veh.getTransmission(),
            		                    order + 2);
            list.add(det);
            det = new DetailDisplay(this.quoteNo,
            		                    "",
            		                    veh.getEngineCap() + " " + veh.getEngineDesc()
            		                    + " " + veh.getCylinders() + "cyl",
            		                    order + 3);
            list.add(det);
            det = new DetailDisplay(this.quoteNo,
            		                    "NVIC",
            		                    nvic,
            		                    order + 4);
			list.add(det);
    }
    
	public Integer getQuoteNo() {
		return quoteNo;
	}

	public void setQuoteNo(Integer quoteNo) {
		this.quoteNo = quoteNo;
	}
	
	public ArrayList<DetailDisplay> getQuoteDetails() {
		return quoteDetails;
	}

	public void setQuoteDetails(ArrayList<DetailDisplay> quoteDetails) {
		this.quoteDetails = quoteDetails;
	}

	public ArrayList<WebInsClient> getClientList() {
		return clientList;
	}

	public void setClientList(ArrayList<WebInsClient> clientList) {
		this.clientList = clientList;
	}

	public WebInsQuote getQuote() {
		return quote;
	}

	public void setQuote(WebInsQuote quote) {
		this.quote = quote;
	}
	
   private class DetailDisplay extends WebInsQuoteDetail implements Comparable<DetailDisplay>
    {
       private static final long serialVersionUID = 7560027193928281423L;
       
		public int compareTo(DetailDisplay o) {
			DetailDisplay det = (DetailDisplay)o;
			return this.displaySeq - det.displaySeq;
		}
		public int displaySeq;
		
		public DetailDisplay(Integer quoteNo,
				             String fieldName,
				             String value,
				             int order)
		{
			super(quoteNo,fieldName,value);
			this.displaySeq = order;
		}
		public void setDisplaySeq(int ds)
		{
			this.displaySeq = ds;
		}
    }

public Map<String,Object> getSession() {
	return session;
}

public void setSession(Map<String,Object> session) {
	this.session = session;
}

public boolean isOwner() {
	return this.owner;
}

public void setOwner(boolean owner) {
	this.owner = owner;
}
/*For clients with RACT Client numbers only,
 * check if address is different to that 
 * stored in the client database
 */
private String addressChanged(WebClient clt)throws Exception
{
	Integer ractCltNo = clt.getRactClientNo();
	String rString = "";
	if(ractCltNo==null
	   || ractCltNo.equals(new Integer(0)))return null; 
	ClientVO rClt = this.cltMgr.getClient(ractCltNo);
	//postal address first
	if(!rClt.getPostStsubId().equals(clt.getPostStsubid())
		|| (rClt.getPostProperty()!=null && !rClt.getPostProperty().equals(clt.getPostProperty()))
		|| (clt.getPostStreetChar()!=null && !rClt.getPostStreetChar().equals(clt.getPostStreetChar())))
	{
	   	rString = "Consultant to change address;";
	}
	else if(!rClt.getResiStsubId().equals(clt.getResiStsubid())
		|| (clt.getResiStreetChar()!=null && !rClt.getResiStreetChar().equals(clt.getResiStreetChar())))
	{
	   	rString += "Consultant to change address;";
	}
	return rString;
}

   public static String getCurrentPolicies(Integer ractClientNo)throws Exception
   {
	    String currentPolicyList = null;
//		Collection<Policy> polList = ractInsMgr.getClientPolicies(ractClientNo, false, false,false);
//		if (polList.size() > 0)
//		{ 
//			Iterator<Policy> it = polList.iterator();
//			while (it.hasNext())
//			{
//				Policy pol = it.next();
//				if(pol.getPolicyStatus().equalsIgnoreCase("current")
//				  || pol.getPolicyStatus().equalsIgnoreCase("renewal")
//				// || pol.getPolicyStatus().equalsIgnoreCase("lapsed")
//						)
//				{
//				    if(currentPolicyList==null)currentPolicyList=pol.getPolicyNumber() + "";
//				    else if (!currentPolicyList.equals(""))
//					   currentPolicyList += ",";
//				       currentPolicyList += pol.getPolicyNumber();
//				}
//			}
//		}
	   return currentPolicyList;
   }
   
   public void addSpecifiedItems(ArrayList<DetailDisplay> list, Integer quoteNo)throws Exception
   {
	   ArrayList<InRiskSi> siList = bean.getSpecifiedItems(quoteNo);
	   String indent = "";
	   int yy = 1000;
	   if(siList!=null
		  && siList.size() > 0)
	   {
		   DetailDisplay ds = new DetailDisplay(quoteNo,
				                                "SPECIFIED ITEMS:",
				                                "",
				                                yy);
		   list.add(ds);
		   for(int xx = 0;xx<siList.size();xx++)
		   {
			   InRiskSi si = siList.get(xx);
			   for(int zz = 0;zz<si.getLineCount();zz++)
			   {
				   yy++;
				   if(zz==0) indent = "* ";
				   else indent = "** ";
				   InRiskSiLineItem li = si.getLineItem(new Integer(zz));
				   ds = new DetailDisplay(quoteNo,
						                  indent + li.getDescription(),
						                  formatSumIns(li.getSumIns()),
						                  yy);
				   list.add(ds);
			   }
		   }
	   }
   }
   private String formatSumIns(BigDecimal sumIns)
   {
	   NumberFormat nf = NumberFormat.getIntegerInstance();
	   return nf.format(sumIns.doubleValue());
   }

	public Map<String,String> getReason() {
		return reason;
	}

	public void setReason(Map<String,String> reason) {
		this.reason = reason;
	}

	public boolean isAdministrator() {
		return administrator;
	}

	public void setAdministrator(boolean administrator) {
		this.administrator = administrator;
	}

	public boolean isDisableInsuranceConversion() {
		return disableInsuranceConversion;
	}

	public void setDisableInsuranceConversion(boolean disableInsuranceConversion) {
		this.disableInsuranceConversion = disableInsuranceConversion;
	}
}
