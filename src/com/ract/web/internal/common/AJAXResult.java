package com.ract.web.internal.common;

import java.io.PrintWriter;

import org.apache.struts2.ServletActionContext;

import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.Result;
import com.opensymphony.xwork2.util.ValueStack;
import com.ract.util.LogUtil;

public class AJAXResult implements Result
{
	public static final String DEFAULT_PARAM = "classAlias";

	private String classAlias;

	public void execute(ActionInvocation invocation) throws Exception
	{
		ServletActionContext.getResponse().setContentType("text/plain");
		PrintWriter responseStream = ServletActionContext.getResponse().getWriter();
		ValueStack valueStack = invocation.getStack();
		Object conversionResponse = valueStack.findValue("conversionResponse");
		LogUtil.debug(this.getClass(), "conversionResponse=" + conversionResponse);
		responseStream.println(conversionResponse.toString());
	}

	public String getClassAlias()
	{
		return classAlias;
	}

	public void setClassAlias(String classAlias)
	{
		this.classAlias = classAlias;
	}

}
