package com.ract.web.internal.security;

import com.ract.user.*;
/**
 * Simple interface for actions which need to have the session User injected
 * @author dgk1
 *
 */
public interface UserAware 
{
   public  void setUser(User user);
}
