package com.ract.web.internal.security;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts2.StrutsStatics;
import org.omg.CORBA.Request;

import com.opensymphony.xwork2.Action;
import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ValidationAware;
import com.opensymphony.xwork2.interceptor.Interceptor;
import com.ract.user.*;
import com.ract.util.LogUtil;

public class AuthenticationInterceptor implements Interceptor
{

	public static final String REDIRECT_ACTION = "redirectAction";

	public void destroy(){}
   public void init(){}
   

   
   public String intercept(ActionInvocation actionInvocation) throws Exception
   {
	   Map session = actionInvocation.getInvocationContext().getSession();
	   User user = (User)session.get("user");
	   if(user==null)
	   {
	  	 session.put(REDIRECT_ACTION,    
	  		    actionInvocation.getInvocationContext().getName());	 

	  	 addActionError(actionInvocation, "You must be authenticated to access this page");

       return Action.LOGIN;
	   }
	   else
	   {
		   Action action = (Action)actionInvocation.getAction();
		   if(action instanceof UserAware)
		   {
			   ((UserAware)action).setUser(user);
		   }
		   return actionInvocation.invoke();
	   }
   }
   
 	private void addActionError(ActionInvocation invocation, String message) {
		Object action = invocation.getAction();
		if(action instanceof ValidationAware) {
			((ValidationAware) action).addActionError(message);
		}
	}   
}
