package com.ract.web.internal.security;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ActionContext;
import com.ract.common.ServiceLocator;
import com.ract.common.integration.RACTPropertiesProvider;
import com.ract.user.*;
import java.util.*;

import org.apache.struts2.interceptor.SessionAware;

import com.ract.common.*;
import com.ract.util.*;
//import javax.servlet.ServletException;

/**
 * <p>
 * Validate a user login.
 * </p>
 */
public class Login extends ActionSupport implements SessionAware
{
	public static final String USER_ID = "userId";

	public String getNextAction()
	{
		return nextAction;
	}



	public void setNextAction(String nextAction)
	{
		this.nextAction = nextAction;
	}



	private String userId = null;
	private String password = null;
	
	private String nextAction;
	
	private Map session = null;

	public String execute() throws Exception
	{
LogUtil.debug(this.getClass(),"\n-----------------------------------"
		         + "\n userId  " + this.userId
		         + "\n-------------------------------------");	
		com.ract.user.User user = this.getUser(this.userId);
		if(user==null)
		{
			addActionError("Invalid user name");
			return ERROR;
		}
		else if (!this.password.equals(user.getPassword()))
		{
			addActionError("Invalid password! Please try again!");
			return ERROR;
		} else
		{
		    session.put("user",user);
		    session.put("userName",user.getUsername());
		    session.put(USER_ID, user.getUserID());
		    session.put("salesBranchCode", user.getSalesBranch().getSalesBranchCode());
		    session.put("printerGroup", user.getPrinterGroup());
		    session.put("sessionStart",(new DateTime()).formatLongDate());
		    
		    this.setSystemSettings();
		    
		    String redirectAction = (String)session.get(AuthenticationInterceptor.REDIRECT_ACTION);
		    session.remove(AuthenticationInterceptor.REDIRECT_ACTION);
		    setNextAction(redirectAction);
		    LogUtil.log(this.getClass(), "redirectAction="+redirectAction);		    
		    
			return SUCCESS;
		}
	}

	
	
	public String getUserId()
	{
		return userId;
	}

	public void setUserId(String value)
	{
		userId = value;
	}


	public String getPassword()
	{
		return password;
	}

	public void setPassword(String value)
	{
		password = value;
	}
	
	
	private com.ract.user.User getUser(String userId)	throws Exception
	{
		UserMgr uMgr = null;
		com.ract.user.User user = null;
		LogUtil.debug(this.getClass(),"userId="+userId);		
		try
		{
			uMgr = UserEJBHelper.getUserMgr();
			user = uMgr.getUser(userId);
			LogUtil.debug(this.getClass(),"user="+user);
		}
		catch(Exception ex)
		{
			throw new Exception("Unable to get user.", ex);
		}
		return user;
	}
	
	private void setSystemSettings() throws Exception
	{
        String systemName = null; 
		String backgroundColour = "";
		CommonMgr commonMgr = CommonEJBHelper.getCommonMgr();
		systemName = commonMgr.getSystemParameterValue(SystemParameterVO.CATEGORY_COMMON,SystemParameterVO.TYPE_SYSTEM);
		if (CommonConstants.SYSTEM_DEVELOPMENT.equals(systemName))
		{
		  backgroundColour = "GREEN";
		}
		else if (CommonConstants.SYSTEM_TEST.equals(systemName))
		{
		  backgroundColour = "ORANGE";
		}
		else if (CommonConstants.SYSTEM_PRODUCTION.equals(systemName))
		{
		  //normal colour
		  backgroundColour = "#CDD7E1";
		}
		session.put("systemName", systemName);
		session.put("backgroundColour", backgroundColour);
	}



	@Override
	public void setSession(Map session) {
		this.session = session;
	}


}
