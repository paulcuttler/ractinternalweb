package com.ract.web.internal.membership;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

import org.apache.struts2.interceptor.SessionAware;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ejb3plugin.InjectEJB;
import com.ract.common.CommonMgrLocal;
import com.ract.common.SalesBranchVO;
import com.ract.user.User;
import com.ract.user.UserMgrLocal;
import com.ract.web.internal.security.Login;
import com.ract.web.membership.WebMembershipMgrLocal;
import com.ract.web.membership.WebMembershipTransactionHeader;

public class ViewUpdateMembershipAction extends ActionSupport implements SessionAware
{

	public boolean isAdministrator()
	{
		return administrator;
	}

	public void setAdministrator(boolean administrator)
	{
		this.administrator = administrator;
	}

	public String getConversionStatus()
	{
		return conversionStatus;
	}

	public void setConversionStatus(String conversionStatus)
	{
		this.conversionStatus = conversionStatus;
	}

	public String getAllocatedUserId()
	{
		return allocatedUserId;
	}

	public void setAllocatedUserId(String allocatedUserId)
	{
		this.allocatedUserId = allocatedUserId;
	}

	public String getSalesBranchCode()
	{
		return salesBranchCode;
	}

	public void setSalesBranchCode(String salesBranchCode)
	{
		this.salesBranchCode = salesBranchCode;
	}

	public String getAllocatedUser()
	{
		return allocatedUser;
	}

	public void setAllocatedUser(String allocatedUser)
	{
		this.allocatedUser = allocatedUser;
	}

	public String getSalesBranch()
	{
		return salesBranch;
	}

	public void setSalesBranch(String salesBranch)
	{
		this.salesBranch = salesBranch;
	}

	public WebMembershipTransactionHeader getWebMembershipTransactionHeader()
	{
		return webMembershipTransactionHeader;
	}

	public void setWebMembershipTransactionHeader(
			WebMembershipTransactionHeader webMembershipTransactionHeader)
	{
		this.webMembershipTransactionHeader = webMembershipTransactionHeader;
	}

	public Collection<String> getConversionStatusList()
	{
		return conversionStatusList;
	}

	public void setConversionStatusList(Collection<String> conversionStatusList)
	{
		this.conversionStatusList = conversionStatusList;
	}

	public Collection<SalesBranchVO> getAllocatedBranchList()
	{
		return allocatedBranchList;
	}

	public void setAllocatedBranchList(Collection<SalesBranchVO> allocatedBranchList)
	{
		this.allocatedBranchList = allocatedBranchList;
	}

	public Collection<User> getAllocatedUserList()
	{
		return allocatedUserList;
	}

	public void setAllocatedUserList(Collection<User> allocatedUserList)
	{
		this.allocatedUserList = allocatedUserList;
	}

	public Map getSession()
	{
		return session;
	}

	private Map session;	
	
	public void setSession(Map session)
	{
		this.session = session;
	}	
	
	public Integer getWebMembershipNo()
	{
		return webMembershipNo;
	}

	public void setWebMembershipNo(Integer webMembershipNo)
	{
		this.webMembershipNo = webMembershipNo;
	}

	private Collection<String> conversionStatusList;
	
	private WebMembershipTransactionHeader webMembershipTransactionHeader;
	
	private Integer webMembershipNo;
	
	@InjectEJB(name="WebMembershipMgr")	
	private WebMembershipMgrLocal webMembershipMgr;
	
	@InjectEJB(name="UserMgrBean")	
	private UserMgrLocal userMgrLocal;		
	
	private Collection<SalesBranchVO> allocatedBranchList;
	
	private Collection<User> allocatedUserList;
	
	private String conversionStatus;
	
	private boolean administrator;
	
	@Override
	public String execute() throws Exception
	{

	  String userId = (String)session.get(Login.USER_ID);	
	  User user = userMgrLocal.getUser(userId);		
	  administrator = user.isPrivilegedUser(SaveMembershipAction.PRIVILEGE_WEB_CONVERSION_ADMINISTRATOR);
		
    conversionStatusList = WebMembershipHelper.getConversionStatusList();

		webMembershipTransactionHeader = webMembershipMgr.getWebMembershipTransactionHeader(webMembershipNo);

		conversionStatus = webMembershipTransactionHeader.getConversionStatus();
		
		allocatedBranchList = commonMgrLocal.getSalesBranchList();
		allocatedUserList = userMgrLocal.getActiveUsers();
		
    if (webMembershipTransactionHeader.getUserName() != null &&
    		!webMembershipTransactionHeader.getUserName().equals(""))
    {
      User allocated = userMgrLocal.getUser(webMembershipTransactionHeader.getUserName());
      allocatedUser = allocated.getUsername();
      allocatedUserId = allocated.getUserID();
    }
    if (webMembershipTransactionHeader.getSalesBranch() != null &&
    		!webMembershipTransactionHeader.getSalesBranch().equals(""))
    {
    	SalesBranchVO branch = commonMgrLocal.getSalesBranch(webMembershipTransactionHeader.getSalesBranch());
      salesBranch = branch.getSalesBranchName();
      salesBranchCode = branch.getSalesBranchCode();
    }		
		
		return SUCCESS;
	}

	private String allocatedUserId;
	
	private String salesBranchCode;
	
	private String allocatedUser;
	
	private String salesBranch;
	
	@InjectEJB(name="CommonMgrBean")	
	private CommonMgrLocal commonMgrLocal;	
	
}
