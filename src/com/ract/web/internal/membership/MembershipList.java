package com.ract.web.internal.membership;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Properties;

import javax.ejb.EJB;
import javax.naming.InitialContext;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ejb3plugin.InjectEJB;
import com.ract.common.GenericException;
import com.ract.common.SourceSystem;
import com.ract.web.insurance.WebInsQuote;
import com.ract.web.membership.WebMembershipMgr;
import com.ract.web.membership.WebMembershipMgrLocal;
import com.ract.web.membership.WebMembershipMgrRemote;
import com.ract.web.membership.WebMembershipTransactionHeader;

public class MembershipList extends ActionSupport
{
	
	//@todo rationalise
	public static final String SALES_BRANCH_WEB = "WB";
	
	public String getWebMembershipNo()
	{
		return webMembershipNo;
	}

	public void setWebMembershipNo(String webMembershipNo)
	{
		this.webMembershipNo = webMembershipNo;
	}

	@InjectEJB(name="WebMembershipMgr")
	private WebMembershipMgrLocal webMembershipMgr;
	
	private String webMembershipNo;
		
  public Collection<WebMembershipTransactionHeader> getMembershipList()
	{
		return membershipList;
	}

	public void setMembershipList(
			Collection<WebMembershipTransactionHeader> membershipList)
	{
		this.membershipList = membershipList;
	}
 
	public String execute()throws Exception
	{   
		
		try
		{
			membershipList = new ArrayList();
			if (webMembershipNo == null ||
					webMembershipNo.trim().equals(""))
			{
				WebMembershipTransactionHeader wmth  = null;
			  membershipList = webMembershipMgr.getCompletedWebMembershipTransactionHeaders();
			  for (Iterator<WebMembershipTransactionHeader> i = membershipList.iterator();i.hasNext();)
			  {
			  	wmth = i.next();
			  	allocateSalesBranch(wmth);	    	
			  }
			}
			else
			{
				WebMembershipTransactionHeader wmth = webMembershipMgr.getWebMembershipTransactionHeader(new Integer(webMembershipNo));
				if (wmth != null)
				{
					membershipList.add(wmth);
				}
				allocateSalesBranch(wmth);
			}
		}
		catch (Exception e)
		{
      e.printStackTrace();
      return ERROR;
		}
	  System.out.println("webMembershipNo="+webMembershipNo);
	 	return SUCCESS;
	}

	private void allocateSalesBranch(WebMembershipTransactionHeader wmth)
			throws GenericException
	{
		if (!wmth.isAllocated())
		{
			//String salesBranchCode = webMembershipMgr.allocateBranch(SourceSystem.MEMBERSHIP.getAbbreviation());
			wmth.setSalesBranch(SALES_BRANCH_WEB);
			webMembershipMgr.updateWebMembershipTransactionHeader(wmth);
		}
	}

	private Collection<WebMembershipTransactionHeader> membershipList;
  
  
  
}
