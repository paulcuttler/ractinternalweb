package com.ract.web.internal.membership;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;
import java.util.Properties;

import javax.naming.InitialContext;

import org.apache.struts2.interceptor.SessionAware;
import org.jasypt.exceptions.EncryptionOperationNotPossibleException;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ejb3plugin.InjectEJB;
import com.ract.common.CommonMgrLocal;
import com.ract.common.GenericException;
import com.ract.user.User;
import com.ract.user.UserMgrLocal;
import com.ract.web.internal.security.Login;
import com.ract.web.membership.WebMembershipMgrLocal;
import com.ract.web.membership.WebMembershipMgrRemote;
import com.ract.web.membership.WebMembershipTransaction;
import com.ract.web.membership.WebMembershipTransactionContainer;
import com.ract.web.membership.WebMembershipTransactionHeader;
import com.ract.web.payment.WebMembershipPayment;

public class WebMembershipDetails extends ActionSupport implements SessionAware {

	public boolean isOwner() {
		return owner;
	}

	public void setOwner(boolean owner) {
		this.owner = owner;
	}

	public boolean isAdministrator() {
		return administrator;
	}

	public void setAdministrator(boolean administrator) {
		this.administrator = administrator;
	}

	public String getSalesBranch() {
		return salesBranch;
	}

	public void setSalesBranch(String salesBranch) {
		this.salesBranch = salesBranch;
	}

	public String getAllocatedUser() {
		return allocatedUser;
	}

	public void setAllocatedUser(String allocatedUser) {
		this.allocatedUser = allocatedUser;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public Map getSession() {
		return session;
	}

	public void setSession(Map session) {
		this.session = session;
	}

	private String userId;

	private User user;

	private Map session;

	public WebMembershipPayment getWebMembershipPayment() {
		return webMembershipPayment;
	}

	private boolean administrator;

	private boolean owner;

	public void setWebMembershipPayment(WebMembershipPayment webMembershipPayment) {
		this.webMembershipPayment = webMembershipPayment;
	}

	public WebMembershipTransactionHeader getWebMembershipTransactionHeader() {
		return webMembershipTransactionHeader;
	}

	public void setWebMembershipTransactionHeader(WebMembershipTransactionHeader webMembershipTransactionHeader) {
		this.webMembershipTransactionHeader = webMembershipTransactionHeader;
	}

	public String getTransactionHeaderId() {
		return transactionHeaderId;
	}

	public void setTransactionHeaderId(String transactionHeaderId) {
		this.transactionHeaderId = transactionHeaderId;
	}

	public Collection<WebMembershipTransaction> getWebMembershipTransactionList() {
		return webMembershipTransactionList;
	}

	public void setWebMembershipTransactionList(Collection<WebMembershipTransaction> webMembershipTransactionList) {
		this.webMembershipTransactionList = webMembershipTransactionList;
	}

	private String transactionHeaderId;

	@InjectEJB(name = "WebMembershipMgr")
	private WebMembershipMgrLocal webMembershipMgr;

	@InjectEJB(name = "UserMgrBean")
	private UserMgrLocal userMgrLocal;

	@InjectEJB(name = "CommonMgrBean")
	private CommonMgrLocal commonMgrLocal;

	private WebMembershipTransactionHeader webMembershipTransactionHeader;

	private Collection<WebMembershipTransaction> webMembershipTransactionList;

	private WebMembershipPayment webMembershipPayment;

	public String execute() throws Exception {
		userId = (String) session.get(Login.USER_ID);

		user = userMgrLocal.getUser(userId);

		administrator = user.isPrivilegedUser(SaveMembershipAction.PRIVILEGE_WEB_CONVERSION_ADMINISTRATOR);

		System.out.println("transactionHeaderId=" + transactionHeaderId);

		WebMembershipTransactionContainer wmtc = null;

		try {
			wmtc = webMembershipMgr.getWebMembershipTransactionContainer(new Integer(transactionHeaderId));
		} catch (GenericException ex) {
			addActionError("This web membership cannot be processed as it was created prior to the introduction of card number encryption.");
		}
		
		if (wmtc != null) {
			webMembershipTransactionHeader = wmtc.getWebMembershipTransactionHeader();
			String allUser = webMembershipTransactionHeader.getUserName();
			if (allUser == null || allUser.equals("")) {
				// set it
				if (!user.getSalesBranchCode().equalsIgnoreCase(webMembershipTransactionHeader.getSalesBranch())) {
					addActionError("Membership has been allocated to sales branch " + webMembershipTransactionHeader.getSalesBranch() + ". You may not be allocated the membership as you belong to sales branch " + user.getSalesBranchCode());
				} else {
					// sales branches match/unallocated
					webMembershipTransactionHeader.setUserName(userId);
					webMembershipMgr.updateWebMembershipTransactionHeader(webMembershipTransactionHeader);
					owner = true;
				}
	
			} else if (!allUser.equalsIgnoreCase(this.userId)) {
				//
				addActionError("Membership has already been allocated to user " + allUser + ". You are unable to convert the web membership.");
			} else if (allUser.equalsIgnoreCase(this.userId)) {
				owner = true;
			}
	
			if (webMembershipTransactionHeader.getUserName() != null && !webMembershipTransactionHeader.getUserName().equals("")) {
				User allocated = userMgrLocal.getUser(webMembershipTransactionHeader.getUserName());
				allocatedUser = allocated.getUsername();
			}
			if (webMembershipTransactionHeader.getSalesBranch() != null && !webMembershipTransactionHeader.getSalesBranch().equals("")) {
				salesBranch = commonMgrLocal.getSalesBranch(webMembershipTransactionHeader.getSalesBranch()).getSalesBranchName();
			}
	
			webMembershipTransactionList = wmtc.getTransactionList();
			webMembershipPayment = webMembershipMgr.getWebMembershipPayment(transactionHeaderId);
		}

		System.out.println(webMembershipPayment.toString());
		
		return SUCCESS;

	}

	private String salesBranch;

	private String allocatedUser;

}
