package com.ract.web.internal.membership;

import java.text.SimpleDateFormat;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ejb3plugin.InjectEJB;
import com.ract.common.ExceptionHelper;
import com.ract.util.DateTime;
import com.ract.util.Interval;
import com.ract.util.LogUtil;
import com.ract.web.client.WebClient;
import com.ract.web.membership.WebMembershipMgrLocal;
import com.ract.web.membership.WebMembershipTransaction;
import com.ract.web.membership.WebMembershipTransactionContainer;
import com.ract.web.membership.WebMembershipTransactionHeader;

public class SearchTemporaryMembershipAction extends ActionSupport
{

  public String getMemberAction()
	{
		return memberAction;
	}

	public void setMemberAction(String memberAction)
	{
		this.memberAction = memberAction;
	}

	private static final String ELIGIBLE_NO = "No";

	private static final String ELIGIBLE_YES = "Yes";

	public String getEligibleForRoadservice()
	{
		return eligibleForRoadservice;
	}

	public void setEligibleForRoadservice(String eligibleForRoadservice)
	{
		this.eligibleForRoadservice = eligibleForRoadservice;
	}

	public WebMembershipTransaction getWebMembershipTransaction()
	{
		return webMembershipTransaction;
	}

	public void setWebMembershipTransaction(
			WebMembershipTransaction webMembershipTransaction)
	{
		this.webMembershipTransaction = webMembershipTransaction;
	}

	public String getDisplayName()
	{
		return displayName;
	}

	public void setDisplayName(String displayName)
	{
		this.displayName = displayName;
	}

	public WebClient getWebClient()
	{
		return webClient;
	}

	public void setWebClient(WebClient webClient)
	{
		this.webClient = webClient;
	}

	public boolean isSearched()
	{
		return searched;
	}

	public void setSearched(boolean searched)
	{
		this.searched = searched;
	}

	public String getTempMembershipNo()
	{
		return tempMembershipNo;
	}

	public void setTempMembershipNo(String tempMembershipNo)
	{
		this.tempMembershipNo = tempMembershipNo;
	}

	@InjectEJB(name="WebMembershipMgr")	
	private WebMembershipMgrLocal webMembershipMgr;	
	
	private String tempMembershipNo;
	
	private WebClient webClient;
	
	private WebMembershipTransaction webMembershipTransaction;
	
	private boolean searched;
	
	private String displayName;
	
	private String eligibleForRoadservice;
	
	private DateTime now;
	
	public DateTime getNow()
	{
		return now;
	}

	public void setNow(DateTime now)
	{
		this.now = now;
	}

	@Override
	public String execute() throws Exception
	{
		
		try
		{
			if (tempMembershipNo != null &&
					!tempMembershipNo.equals(""))
			{
				tempMembershipNo = tempMembershipNo.toUpperCase();
				int start = tempMembershipNo.indexOf(WebMembershipTransaction.PREFIX_TEMPORARY_MEMBERSHIP_NUMBER);
				if (start == -1)
				{
					addActionError("Temporary membership numbers should start with \""+WebMembershipTransaction.PREFIX_TEMPORARY_MEMBERSHIP_NUMBER+"\"");
					return INPUT;
				}
				int end = tempMembershipNo.length();
				if (end < 8)
				{
					addActionError("Temporary membership numbers should have 8 characters. eg. T0001234");
					return INPUT;					
				}
				
				String webMembershipNo = tempMembershipNo.substring(start+1,end);
				
				LogUtil.debug(this.getClass(),"webMembershipNo=["+webMembershipNo+"]");
				LogUtil.debug(this.getClass(),"tempMembershipNo=["+tempMembershipNo+"]");			
				
				Integer transactionId = new Integer(webMembershipNo);
				
				webMembershipTransaction = webMembershipMgr.getWebMembershipTransaction(transactionId);
				if (webMembershipTransaction != null)
				{
					WebMembershipTransactionContainer wmtc = webMembershipMgr.getWebMembershipTransactionContainer(webMembershipTransaction.getWebMembershipTransactionPK().getTransactionHeaderId()); 
					WebMembershipTransactionHeader webMembershipTransactionHeader = wmtc.getWebMembershipTransactionHeader();
					if (webMembershipTransactionHeader.getCompletedDate() != null)
					{
						
						DateTime transDateTime = webMembershipTransaction.getCreateDate();
						
						SimpleDateFormat df = new SimpleDateFormat("HHmmss");
						String time = df.format(transDateTime);
						int hrs = Integer.parseInt(time.substring(0,2));
						LogUtil.log(this.getClass(), "hrs"+hrs);
						int mins = Integer.parseInt(time.substring(2,4));
						LogUtil.log(this.getClass(), "mins"+mins);						
						int secs = Integer.parseInt(time.substring(4,6));
						LogUtil.log(this.getClass(), "secs"+secs);						
						Interval timecomp = new Interval(0,0,0,hrs,mins,secs,0);
						LogUtil.log(this.getClass(), "timecomp"+timecomp);
						DateTime startDate = webMembershipTransaction.getStartDate();
						startDate = startDate.add(timecomp);
						webMembershipTransaction.setStartDate(startDate);						
						LogUtil.log(this.getClass(), "startDate"+startDate.formatLongDate());
						DateTime endDate = webMembershipTransaction.getEndDate();
						endDate = endDate.add(timecomp);
						webMembershipTransaction.setEndDate(endDate);
						LogUtil.log(this.getClass(), "endDate"+endDate.formatLongDate());

						now = new DateTime(); //11/11/2001

						LogUtil.log(this.getClass(), "now"+now.formatLongDate());
						LogUtil.log(this.getClass(), "startDate"+startDate.formatLongDate());
						LogUtil.log(this.getClass(), "endDate"+endDate.formatLongDate());
						
						if (now.after(startDate) && //10/11/2001
								now.before(endDate)) //10/11/2002
						{
							LogUtil.log(this.getClass(), "eligible");							
							eligibleForRoadservice = ELIGIBLE_YES;
						}
						else
						{
							eligibleForRoadservice = ELIGIBLE_NO;
						}
						
						webClient = webMembershipTransaction.getWebClient();
						displayName = webClient.getDisplayName();
						
						String memberNumber = null;
						if (webClient.getRactClientNo() != null)
						{
						  memberNumber = webClient.getRactClientNo().toString();
						}
						
						//Never to convert
						if (webMembershipTransactionHeader.getConversionStatus() != null &&
								webMembershipTransactionHeader.getConversionStatus().equals(WebMembershipTransactionHeader.STATUS_NEVER_CONVERT))
						{
							memberAction = "Error with online transaction. JOS applicable. Refer to admin if required.";
							eligibleForRoadservice = ELIGIBLE_NO;						
						}
						else //Manually Converted or Converted
						{
							if (eligibleForRoadservice.equals(ELIGIBLE_YES))
							{
								if (memberNumber != null)
								{
									memberAction = " Create member road service job for RACT member "+memberNumber;
								}
								else
								{
									memberAction = "Create road service job for interim membership.";
								}
							}
							else
							{
								memberAction = "JOS applicable.";
							}
						}
						
					}
					else
					{
						//ensure null as not a completed membership transaction
						webMembershipTransaction = null;
					}
				}
				searched = true;
			}
			else
			{
				addActionError("No search term entered.");
				return INPUT;
			}

		}
		catch (Exception e)
		{
			addActionError(ExceptionHelper.getExceptionStackTrace(e));
			return ERROR;
		}
		
		return SUCCESS;
	}
	
	private String memberAction;
	
}
