package com.ract.web.internal.membership;

import java.util.Map;

import org.apache.struts2.interceptor.SessionAware;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ejb3plugin.InjectEJB;
import com.ract.common.ExceptionHelper;
import com.ract.util.LogUtil;
import com.ract.web.convert.membership.*;
import com.ract.web.internal.security.Login;
import com.ract.web.membership.WebMembershipClient;
import com.ract.web.membership.WebMembershipMgrLocal;
import com.ract.web.membership.WebMembershipTransactionContainer;

public class ConvertMembershipAction extends ActionSupport implements SessionAware
{

	
	public String getConversionResponse()
	{
		return conversionResponse;
	}

	public void setConversionResponse(String conversionResponse)
	{
		this.conversionResponse = conversionResponse;
	}
 
	@InjectEJB(name="WebMembershipMgr")	
	private WebMembershipMgrLocal webMembershipMgr;
	
	@InjectEJB(name="MembershipConversionMgr")	
	private MembershipConversionMgrLocal membershipConversionMgr;

	public Map getSession()
	{
		return session;
	}

	public void setSession(Map session)
	{
		this.session = session;
	}

	private Map session;	
	
	public Integer getWebMembershipNo()
	{
		return webMembershipNo;
	}

	private String conversionResponse;
	
	public void setWebMembershipNo(Integer webMembershipNo)
	{
		this.webMembershipNo = webMembershipNo;
	}

	public Integer webMembershipNo;
	
	@Override
	public String execute() throws Exception
	{
	  String userId = (String)session.get(Login.USER_ID);

		LogUtil.debug(this.getClass(), "webMembershipNo="+webMembershipNo);
		LogUtil.debug(this.getClass(), "userId="+userId);		
	  
	  try
		{
		    LogUtil.info(this.getClass(), "Start conversion");
			conversionResponse = membershipConversionMgr.convertMembership(webMembershipNo.toString(), userId);
			LogUtil.info(this.getClass(), "complete conversion");
		}
		catch (Exception e)
		{
			e.printStackTrace();
			conversionResponse = e.getMessage();
			return ERROR;
		}

		return SUCCESS;
	}

}
