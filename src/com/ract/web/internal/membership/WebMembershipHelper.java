package com.ract.web.internal.membership;

import java.util.ArrayList;
import java.util.Collection;

import com.ract.web.membership.WebMembershipTransactionHeader;

public class WebMembershipHelper
{

	public static Collection<String> getConversionStatusList()
	{
		Collection conversionStatusList = new ArrayList<String>();
		conversionStatusList.add(WebMembershipTransactionHeader.STATUS_CONVERTED);
		conversionStatusList.add(WebMembershipTransactionHeader.STATUS_MANUAL);
		conversionStatusList.add(WebMembershipTransactionHeader.STATUS_COVERED);		
		conversionStatusList.add(WebMembershipTransactionHeader.STATUS_NEVER_CONVERT);
		return conversionStatusList;
	}
	
}
