package com.ract.web.internal.membership;

import java.rmi.RemoteException;
import java.util.Collection;
import java.util.Map;

import org.apache.struts2.interceptor.SessionAware;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.Preparable;
import com.opensymphony.xwork2.ValidationAware;
import com.opensymphony.xwork2.ejb3plugin.InjectEJB;
import com.ract.common.CommonMgrLocal;
import com.ract.common.SalesBranchVO;
import com.ract.common.SourceSystem;
import com.ract.payment.PaymentMgrLocal;
import com.ract.payment.receipting.Receipt;
import com.ract.user.User;
import com.ract.user.UserMgrLocal;
import com.ract.util.DateTime;
import com.ract.util.FileUtil;
import com.ract.web.convert.membership.MembershipConversionMgrLocal;
import com.ract.web.internal.security.Login;
import com.ract.web.membership.WebMembershipMgrLocal;
import com.ract.web.membership.WebMembershipTransaction;
import com.ract.web.membership.WebMembershipTransactionContainer;
import com.ract.web.membership.WebMembershipTransactionHeader;
import com.ract.web.payment.WebMembershipPayment;

public class SaveMembershipAction extends ActionSupport implements SessionAware, ValidationAware, Preparable
{
	
	public String getConversionReference()
	{
		return conversionReference;
	}

	public void setConversionReference(String conversionReference)
	{
		this.conversionReference = conversionReference;
	}

	public String getConversionComments()
	{
		return conversionComments;
	}

	public void setConversionComments(String conversionComments)
	{
		this.conversionComments = conversionComments;
	}

	public Collection<SalesBranchVO> getAllocatedBranchList()
	{
		return allocatedBranchList;
	}

	public void setAllocatedBranchList(Collection<SalesBranchVO> allocatedBranchList)
	{
		this.allocatedBranchList = allocatedBranchList;
	}

	public Collection<User> getAllocatedUserList()
	{
		return allocatedUserList;
	}

	public void setAllocatedUserList(Collection<User> allocatedUserList)
	{
		this.allocatedUserList = allocatedUserList;
	}

	public Collection<String> getConversionStatusList()
	{
		return conversionStatusList;
	}

	public void setConversionStatusList(Collection<String> conversionStatusList)
	{
		this.conversionStatusList = conversionStatusList;
	}

	private Collection<SalesBranchVO> allocatedBranchList;
	
	private Collection<User> allocatedUserList;
	
	private Collection<String> conversionStatusList;	
	
	private boolean administrator;
	
	public final static String PRIVILEGE_WEB_CONVERSION_ADMINISTRATOR = "WEBCONVADM";
	
	@Override
	public void prepare() throws Exception
	{		
	  String userId = (String)session.get(Login.USER_ID);	
	  user = userMgrLocal.getUser(userId);				
	  administrator = user.isPrivilegedUser(PRIVILEGE_WEB_CONVERSION_ADMINISTRATOR);		
		
    conversionStatusList = WebMembershipHelper.getConversionStatusList();	
		allocatedBranchList = commonMgrLocal.getSalesBranchList();
		allocatedUserList = userMgrLocal.getActiveUsers();
		
		WebMembershipTransactionContainer wmtc = webMembershipMgr.getWebMembershipTransactionContainer(webMembershipNo);		
	
		
		webMembershipTransactionHeader = wmtc.getWebMembershipTransactionHeader();
    if (webMembershipTransactionHeader.getUserName() != null &&
    		!webMembershipTransactionHeader.getUserName().equals(""))
    {
      User allocated = userMgrLocal.getUser(webMembershipTransactionHeader.getUserName());
      allocatedUser = allocated.getUsername();
    }
    if (webMembershipTransactionHeader.getSalesBranch() != null &&
    		!webMembershipTransactionHeader.getSalesBranch().equals(""))
    {
      salesBranch = commonMgrLocal.getSalesBranch(webMembershipTransactionHeader.getSalesBranch()).getSalesBranchName();
    }
    
		webMembershipTransactionList = wmtc.getTransactionList();
		webMembershipPayment = (WebMembershipPayment)wmtc.getWebPayment();				
		
		
	}

	public User getUser()
	{
		return user;
	}

	public void setUser(User user)
	{
		this.user = user;
	}

	public String getSalesBranch()
	{
		return salesBranch;
	}

	public void setSalesBranch(String salesBranch)
	{
		this.salesBranch = salesBranch;
	}

	public String getSalesBranchCode()
	{
		return salesBranchCode;
	}

	public void setSalesBranchCode(String salesBranchCode)
	{
		this.salesBranchCode = salesBranchCode;
	}

	private User user;
		
	@Override
	public void validate()
	{
		
    if (allocatedUserId != null && salesBranchCode != null)
    {
    	try
			{
				User user = userMgrLocal.getUser(allocatedUserId);
				if (user != null &&
						!user.getSalesBranchCode().equals(salesBranchCode))
				{
					addActionError("User sales branch '"+user.getSalesBranchCode()+"' is not the same as the allocated branch '"+salesBranchCode+"'.");
				}
			}
			catch (RemoteException e)
			{
				addActionError("Error getting user. "+e.getMessage());
			}
    }
    
    //check receipt number attached if manual
    if (WebMembershipTransactionHeader.STATUS_MANUAL.equals(conversionStatus) &&
    		(conversionReference == null ||
    				conversionReference.equals("")))
    {
    	addActionError("Conversion status is manual but no receipt number has been entered.");
    }
    
    if ((WebMembershipTransactionHeader.STATUS_MANUAL.equals(conversionStatus) ||
    		WebMembershipTransactionHeader.STATUS_NEVER_CONVERT.equals(conversionStatus)) &&
    		(conversionComments == null ||
    		 conversionComments.equals("")) )
    {
    	addActionError("Conversion status is manual or never to convert but no comments have been entered.");
    }    
		
    if (conversionReference != null &&
    		!conversionReference.equals(""))
    {
    	Receipt receipt = null;
    	try
    	{
    		String webReceiptingSystem = FileUtil.getProperty("master", "webReceiptingSystem");		
    		receipt = paymentMgrLocal.getReceiptByReceiptId(SourceSystem.getSourceSystem(webReceiptingSystem), conversionReference);
    	}
    	catch (Exception e)
    	{
    		e.printStackTrace();
    		addActionError(e.getMessage());
    	}
    	if (receipt == null)
    	{
    		addActionError("The receipt number '"+conversionReference+"' is not valid.");
    	}
    }
    
	}

	public Collection<WebMembershipTransaction> getWebMembershipTransactionList()
	{
		return webMembershipTransactionList;
	}

	public void setWebMembershipTransactionList(
			Collection<WebMembershipTransaction> webMembershipTransactionList)
	{
		this.webMembershipTransactionList = webMembershipTransactionList;
	}

	public WebMembershipPayment getWebMembershipPayment()
	{
		return webMembershipPayment;
	}

	public void setWebMembershipPayment(WebMembershipPayment webMembershipPayment)
	{
		this.webMembershipPayment = webMembershipPayment;
	}

	public Map getSession()
	{
		return session;
	}

	public void setSession(Map session)
	{
		this.session = session;
	}
	
	private Map session;	
	
	public Integer getWebMembershipNo()
	{
		return webMembershipNo;
	}

	public void setWebMembershipNo(Integer webMembershipNo)
	{
		this.webMembershipNo = webMembershipNo;
	}

	public String getAllocatedUser()
	{
		return allocatedUser;
	}

	public void setAllocatedUser(String allocatedUser)
	{
		this.allocatedUser = allocatedUser;
	}

	public WebMembershipTransactionHeader getWebMembershipTransactionHeader()
	{
		return webMembershipTransactionHeader;
	}

	public void setWebMembershipTransactionHeader(
			WebMembershipTransactionHeader webMembershipTransactionHeader)
	{
		this.webMembershipTransactionHeader = webMembershipTransactionHeader;
	}

	public String getConversionStatus()
	{
		return conversionStatus;
	}

	public void setConversionStatus(String conversionStatus)
	{
		this.conversionStatus = conversionStatus;
	}

	public String getAllocatedUserId()
	{
		return allocatedUserId;
	}

	public void setAllocatedUserId(String allocatedUserId)
	{
		this.allocatedUserId = allocatedUserId;
	}

	@InjectEJB(name="WebMembershipMgr")	
	private WebMembershipMgrLocal webMembershipMgr;	

	@InjectEJB(name="MembershipConversionMgr")	
	private MembershipConversionMgrLocal membershipConversionMgr;	
	
	private String conversionStatus;
	
	private String allocatedUserId;
		
	private Integer webMembershipNo;
	
	private String salesBranchCode;
	
	private String allocatedUser;	
	
	private WebMembershipTransactionHeader webMembershipTransactionHeader; 

	@InjectEJB(name="PaymentMgrBean")	
	private PaymentMgrLocal paymentMgrLocal;		
	
	@InjectEJB(name="UserMgrBean")	
	private UserMgrLocal userMgrLocal;	

	@InjectEJB(name="CommonMgrBean")	
	private CommonMgrLocal commonMgrLocal;			
	
	private Collection<WebMembershipTransaction> webMembershipTransactionList;

	private WebMembershipPayment webMembershipPayment;	
	
	private String conversionReference;
	
	private String conversionComments;
	
	@Override
	public String execute() throws Exception
	{		
				
		String prevStatus = webMembershipTransactionHeader.getConversionStatus();
		if (!prevStatus.equals(conversionStatus))
		{
			webMembershipTransactionHeader.setConversionStatus(conversionStatus);
		  if (WebMembershipTransactionHeader.STATUS_MANUAL.equals(conversionStatus) ||
		      WebMembershipTransactionHeader.STATUS_CONVERTED.equals(conversionStatus) ||
		      WebMembershipTransactionHeader.STATUS_NEVER_CONVERT.equals(conversionStatus))
		  {
		  	webMembershipTransactionHeader.setConversionDate(new DateTime());
		  	webMembershipTransactionHeader.setConversionUserId(user.getUserID());		  	
		  }
		  else if (WebMembershipTransactionHeader.STATUS_COVERED.equals(conversionStatus))
		  {
		  	webMembershipTransactionHeader.setConversionDate(null);	
		  }
		}
		
		webMembershipTransactionHeader.setUserName(allocatedUserId);
		webMembershipTransactionHeader.setSalesBranch(salesBranchCode);
		webMembershipTransactionHeader.setConversionReference(conversionReference);
		webMembershipTransactionHeader.setConversionComments(conversionComments);
		
		webMembershipMgr.updateWebMembershipTransactionHeader(webMembershipTransactionHeader);

		// Bad usage - replaced with encrypted string in database
		// String cardNo = webMembershipPayment.getCardNumber();
		//webMembershipPayment.setCardNumber(membershipConversionMgr.obfuscate(cardNo));
		
		webMembershipMgr.updateWebMembershipPayment(webMembershipPayment);
		
		if (salesBranchCode != null &&
				!salesBranchCode.equals(""))
		{
		  salesBranch = commonMgrLocal.getSalesBranch(salesBranchCode).getSalesBranchName();
		}
		if (allocatedUserId != null &&
				!allocatedUserId.equals(""))
		{
			allocatedUser = userMgrLocal.getUser(allocatedUserId).getUsername();
		}
		return SUCCESS;
	}
	
	private String salesBranch;

}
